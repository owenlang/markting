#ifndef CIEPROXY_H
#define CIEPROXY_H
#include <Wininet.h>

/*
  设置IE代理流程
  wininet.h wininet.lib

1.用HttpQueryInfo查询代理是否需要验证
2.用InternetSetOption(hResourceHandle, INTERNET_OPTION_PROXY_USERNAME
  设置username
3.用InternetSetOption(hResourceHandle, INTERNET_OPTION_PROXY_PASSWORD,
  设置password

示例代码:
BOOL SetProxy(const string &strFullAddr,const string &user, const string &passwd)
{
    BOOL bResult;
    HKEY phKResult;
    DWORD dwcode = 0;
    LPCTSTR pchSubKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
    RegOpenKeyEx(HKEY_CURRENT_USER, pchSubKey, 0, KEY_ALL_ACCESS, &phKResult);    //打开注册表
    int i = 1;
    RegSetValueEx(phKResult, "ProxyServer", 0, REG_SZ,(LPBYTE)strFullAddr.c_str(), strFullAddr.size()+1);//设置注册表键值
    RegCloseKey(phKResult);
    EnableProxy(true);//获得打开代理允许

    bResult = InternetSetOption(NULL, INTERNET_OPTION_SETTINGS_CHANGED, NULL, 0);    //通知注册表中代理改变,下次连接时启动代理
    bResult = InternetSetOption(NULL ,INTERNET_OPTION_REFRESH , NULL, 0);    //从注册表中读入代理
    DWORD size = sizeof(dwcode);
    HttpQueryInfo(NULL, HTTP_QUERY_FLAG_NUMBER |
             HTTP_QUERY_STATUS_CODE, &dwcode, &size , NULL);
    if(dwcode = HTTP_STATUS_PROXY_AUTH_REQ)//设置用户名和密码
    {
       bResult = InternetSetOption(NULL,INTERNET_OPTION_USERNAME,(LPVOID)user.c_str(),user.size()+1);
       bResult = InternetSetOption(NULL,INTERNET_OPTION_USERNAME,(LPVOID)passwd.c_str(),passwd.size()+1);
    }

    return bResult;
}
*/
#if 0
BOOL SetConnectionOptions(LPCTSTR conn_name, LPCTSTR proxy_full_proxy)
{
    //conn_name:active connection name.
    //proxy_full_proxy: eg "210.78.22.87:9000"
    INTERNET_PER_CONN_OPTION_LIST list;
    BOOL bReturn;
    DWORD dwBufSize = sizeof(list);
    //Fill out list struct
    list.dwSize = sizeof(list);
    //NULL == LAN, otherwise connectoid name
    list.pszConnection = conn_name;
    //set three options.
    list.dwOptionCount = 3;
    list.pOptions = new INTERNET_PER_CONN_OPTION[3];
    //make sure the memory was allocated.
    if (NULL == list.pOptions) {
        //Return FALSE if the memory wasn't allocated.
        qDebug("failed to allocat memory in SetConnectionOptions()");
        return FALSE;
    }
    //set flags
    list.pOptions[0].dwOption = INTERNET_PER_CONN_FLAGS;
    list.pOptions[0].Value.dwValue = PROXY_TYPE_DIRECT | PROXY_TYPE_PROXY;
    //set proxy name
    list.pOptions[1].dwOption = INTERNET_PER_CONN_SERVER;
    list.pOptions[1].Value.pszValue = proxy_full_proxy; //"http://proxy:80"
    //set proxy override
    list.pOptions[2].dwOption = INTERNET_PER_CONN_BYPASS;
    list.pOptions[2].Value.pszValue = "local";
    //set the options on the connection.
    bReturn = InternetSetOption(NULL, INTERNET_OPTION_PER_CONNECTION_OPTION,
                                &list, dwBufSize);
    //free the allocated memory.
    delete [] list.pOptions;
    InternetSetOption(NULL, INTERNET_OPTION_SETTINGS_CHANGED, NULL, 0);//通知注册表中代理改变,下次连接时启动代理
    InternetSetOption(NULL, INTERNET_OPTION_REFRESH, NULL, 0);//从注册表中读入代理
    return bReturn;
}
BOOL DisableConnectionProxy(LPCTSTR conn_name)
{
    //conn_name:active connection name.
    INTERNET_PER_CONN_OPTION_LIST list;
    BOOL bReturn;
    DWORD dwBufSize = sizeof(list);
    //Fill out list struct
    list.dwSize = sizeof(list);
    //NULL == LAN, otherwise connectoid name
    list.pszConnection = conn_name;
    //set three options.
    list.dwOptionCount = 1;
    list.pOptions = new INTERNET_PER_CONN_OPTION[list.dwOptionCount];
    //make sure the memory was allocated.
    if (NULL == list.pOptions) {
        //Return FALSE if the memory wasn't allocated.
        qDebug("failed to allocat memory in SetConnectionOptions()");
        return FALSE;
    }
    //set flags
    list.pOptions[0].dwOption = INTERNET_PER_CONN_FLAGS;
    list.pOptions[0].Value.dwValue = PROXY_TYPE_DIRECT;
    //set the options on the connection.
    bReturn = InternetSetOption(NULL, INTERNET_OPTION_PER_CONNECTION_OPTION, &list, dwBufSize);
    //free the allocated memory.
    delete [] list.pOptions;
    InternetSetOption(NULL, INTERNET_OPTION_SETTINGS_CHANGED, NULL, 0);
    InternetSetOption(NULL, INTERNET_OPTION_REFRESH, NULL, 0);
    return bReturn;
}
//set   proxy
const   char*   connection_name="Connection   to   adsl3";
SetConnectionOptions(connection_name,"62.81.236.23:80");
//disable   proxy
DisableConnectionProxy(connection_name);
#endif
class CIEProxy
{
public:
    CIEProxy();
};

#endif // CIEPROXY_H
