#ifndef VARIABLEMANAGER_H
#define VARIABLEMANAGER_H

#include <QWidget>
#include <QMap>

QT_BEGIN_NAMESPACE
class QStandardItemModel;
class QAction;
class QTableWidget;
QT_END_NAMESPACE

class VariableManager : public QWidget
{
    Q_OBJECT
public:
    explicit VariableManager(QWidget *parent = 0);
    static VariableManager* getInstance();
    virtual ~VariableManager();

    QString getVar(const QString& var);
    void setVar(const QString& var, const QString& value);
    QStandardItemModel* getModel();
signals:
    
public slots:
    void newVar();
    void editVar();
    void delVar();
    void clearVar();
    
private:
    void initTable();
    void createActions();

    static VariableManager *self;
    QMap <QString, QString> map_;
    QStandardItemModel *varModel_;
    QTableWidget *varTable_;

    QAction *newAction_;
    QAction *editAction_;
    QAction *delAction_;
    QAction *clearAction_;

};

#endif // VARIABLEMANAGER_H
