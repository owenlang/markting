/*-----------------------------------------------------------------------------
    utils.h -- 一些实用函数的头文件
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#ifndef UTILS_H
#define UTILS_H

class QString;
//转换成中文
const QString cn_tr(const char *str);
//QString 转换为 char*
const char* qs_to_ch(const QString str);
//QString 转换成 BSTR类型
BSTR QStringToBSTR(const QString &str);
//BSTR 转换成 QString类型
QString BSTRToQString(const BSTR bstr);
void DelayMs(int delay_time);


#endif // UTILS_H
