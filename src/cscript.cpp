#include "StdAfx.h"
#include "cscript.h"

//默认脚本类型为无效
CScript::CScript(QObject *parent) :
    QObject(parent), type(XInvalid)
{
}
CScript::CScript(const CScript &script)
{
    this->type = script.type;
    this->map = script.map;
}

CScript& CScript::operator =(const CScript &script)
{
    this->type = script.type;
    this->map = script.map;
    return *this;
}

//需要检查参数是否有效
CScript::CScript(const ScriptType type)
{
    if (type<=XStart || type>=XEnd) {
            this->type = XInvalid;
            return;
    }
    this->type = type;
}

//只要参数有效就加入一个选项
void CScript::add(const QString option, const QString value)
{
    if (this->type == XInvalid) {
        return;
    }
    map.insert(option, value);
}

//删..
void CScript::del(const QString option)
{
    map.remove(option);
}

//查...
QString CScript::get(const QString option)
{
    if (map.contains(option)) {
        return map.value(option);
    } else {
        return QString();
    }
}

//改..
bool CScript::set(const QString option, const QString value)
{
    bool res = false;
    if (map.contains(option)) {
        map[option] = value;
        res = true;
    }
    return res;
}

//选项数量
int CScript::optionCount()
{
    return map.size();
}

//获取本条脚本类型
CScript::ScriptType CScript::getType()
{
    return this->type;
}

//设置本条脚本类型
void CScript::setType(const ScriptType type)
{
    if (type<=XStart || type>=XEnd) {
        this->type = XInvalid;
        return;
    } else {
        this->type = type;
    }
}

QString CScript::getopt(const ScriptType &op)
{
    switch (op) {
        case XAccess: return "XAccess";
        case XFill: return "XFill";
        case XKeyboard: return "XKeyboard";
        case XSend: return "XSend";
        case XCondition: return "XCondition";
        case XVerify: return "XVerify";
        case XMouse: return "XMouse";
        case XJmp: return "XJmp";
        case XScreenshot: return "XScreenshot";
        case XDelay: return "XDelay";
        case XWindow: return "XWindow";
        case Xcollect: return "Xcollect";
        case XUpload: return "XUpload";
    }
    return QString();
}

void CScript::save()
{
}
void CScript::load()
{
}

//打印所有选项信息
void CScript::printAllInfo()
{
    if (map.isEmpty()) {
            qDebug("Error:this script is empty.");
            return;
    }
    qDebug("ScriptType:");
    switch (this->type) {
        case XInvalid:
        case XStart:
        case XEnd:      qDebug("Invalid type."); return;
        case XAccess    :qDebug() << cn_tr("XAccess"); break;
        case XFill	    :qDebug() << cn_tr("XFill"); break;
        case XKeyboard  :qDebug() << cn_tr("XKeyboard"); break;
        case XSend      :qDebug() << cn_tr("XSend"); break;
        case XCondition :qDebug() << cn_tr("XCondition"); break;
        case XVerify    :qDebug() << cn_tr("XVerify"); break;
        case XMouse     :qDebug() << cn_tr("XMouse"); break;
        case XJmp       :qDebug() << cn_tr("XJmp"); break;
        case XScreenshot:qDebug() << cn_tr("XScreenshot"); break;
        case XDelay     :qDebug() << cn_tr("XDelay"); break;
        case XWindow    :qDebug() << cn_tr("XWindow"); break;
        case Xcollect   :qDebug() << cn_tr("Xcollect"); break;
        case XUpload    :qDebug() << cn_tr("XUpload"); break;
    }

    QMap <QString,QString>::iterator it;
    for (it = map.begin(); it != map.end(); ++it) {
        qDebug() << it.key() << ":" << it.value();
    }
}

void CScript::run()
{
    qDebug("Error: run in CScript class");
}

void CScript::stop()
{
    qDebug("Error: stop in CScript class");
}

CScript::RunState CScript::getState()
{
    return this->state;
}

void CScript::setState(RunState state)
{
    this->state = state;
}

CScript::~CScript()
{

}
