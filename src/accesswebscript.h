#ifndef AccessWebScript_H
#define AccessWebScript_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
class CScript;
class QTimer;
class BrowserGet;
QT_END_NAMESPACE

class AccessWebScript : public CScript
{
    Q_OBJECT
public:
    typedef enum{
        AccStart = 0,
                AccUrl, //url
                AccClearAllText,//清除网页所有文本框
                AccClearCookies,//清除cookie
                AccDisablePopup,//禁止弹窗
                AccUseProxy,//使用代理
                AccRefresh,//刷新网页
        AccEnd
    }AccessWebOption;
public:
    explicit AccessWebScript(QObject *parent = 0);
    virtual ~AccessWebScript();
    virtual void run();
    virtual void stop();
    virtual QString getopt(const AccessWebOption &op);
    virtual void save();
    virtual void load();
signals:
    void runFinish(mark::RunDirect rd, int to);
    
public slots:
    void loadComplete();

private:
    void enumFrame(IHTMLDocument2 *pIHTMLDocument2);
    void enumForm(IHTMLDocument2 *pIHTMLDocument2);

    BrowserGet *browserGet_;
};

#endif // AccessWebScript_H
