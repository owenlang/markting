#include "StdAfx.h"
#include "findelement.h"
#include "ui_mainwidget.h"

FindElement::FindElement(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    this->hide();
    browserGet_ = BrowserGet::getInstance();
}

void FindElement::on_findBtn_clicked()
{
    //clear list widget
    listWidget->clear();
    countLabel->setText(QString("%1").arg(0));
    //get keyword
    keyword_ = keywordEdit->text().trimmed();
    if (keyword_.isEmpty() || !browserGet_) {
        htmlText_ = "";
        return;
    }
    //get all html text
    htmlText_ = browserGet_->getHtml();
    if (htmlText_.isEmpty()) {
        return;
    }
    //find keywords in html text
    if (htmlText_.contains(keyword_)) { //keyword is a tag name. like "div" "ul" "li" and so on
        QString exp;
        if (0 == keyword_.compare("input", Qt::CaseInsensitive) //construct the regular expression
        || 0 == keyword_.compare("a", Qt::CaseInsensitive)) {
            exp = "<"+keyword_ + ".*>";        //for matched <input type="button" />
        } else {
            exp = "<"+keyword_ + ".*</" + keyword_ + ">"; //for matched <div id="id">some text</div>
        }
        QRegExp rx(exp, Qt::CaseInsensitive);
        rx.setMinimal(true);    //贪婪匹配
        int pos = 0;
        while ((pos = rx.indexIn(htmlText_, pos)) != -1) {
            QString str = cn_tr(rx.cap(0).toUtf8().data());
            str.replace(QRegExp(">\\s+<"), "><"); //clear write symbol between tag
            str.replace(QRegExp("\\s+"), " ");  //clear other too longer write symbol
            pos += rx.matchedLength();
            listWidget->addItem(new QListWidgetItem(str));
        }

    }
    //update status label
    QString strcount = QString("%1").arg(listWidget->count());
    countLabel->setText(strcount);
}

void FindElement::on_okBtn_clicked()
{
    //get select item from list widget
    QListWidgetItem *item = listWidget->currentItem();
    if (!item) {
        QMessageBox::warning(this, cn_tr("警告"), cn_tr("no select!"));
        return;
    }
    //get info from select item
    QString str = item->text();
                            //<input class="info" id="password_again_info">
    QString tag_name, attr_id, attr_class, attr_name, attr_type, attr_value, attr_src, attr_href, attr_title, attr_content;

    QRegExp rx_tag("<([a-z]+)\\s", Qt::CaseInsensitive);  //get tag name  ...-> div
    rx_tag.setMinimal(true);
    if (-1 != (rx_tag.indexIn(str, 0))) {
        tag_name = rx_tag.cap(1);
    }
    QRegExp rx_class("class=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get class ...-> info
    rx_class.setMinimal(true);
    if (-1 != (rx_class.indexIn(str, 0))) {
        attr_class = rx_class.cap(1);
    }
    QRegExp rx_id("id=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get id ...->password_again_info
    rx_id.setMinimal(true);
    if (-1 != (rx_id.indexIn(str, 0))) {
        attr_id = rx_id.cap(1);
    }
    QRegExp rx_name("name=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get name ...->
    rx_name.setMinimal(true);
    if (-1 != (rx_name.indexIn(str, 0))) {
        attr_name = rx_name.cap(1);
    }
    QRegExp rx_type("type=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get type ...->
    rx_type.setMinimal(true);
    if (-1 != (rx_type.indexIn(str, 0))) {
        attr_type = rx_type.cap(1);
    }
    QRegExp rx_value("value=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get value ...->
    rx_value.setMinimal(true);
    if (-1 != (rx_value.indexIn(str, 0))) {
        attr_value = rx_value.cap(1);
    }
    QRegExp rx_src("src=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get src ...->
    rx_src.setMinimal(true);
    if (-1 != (rx_src.indexIn(str, 0))) {
        attr_src = rx_src.cap(1);
    }
    QRegExp rx_href("href=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get href ...->
    rx_href.setMinimal(true);
    if (-1 != (rx_href.indexIn(str, 0))) {
        attr_href = rx_href.cap(1);
    }
    QRegExp rx_title("title=[\"'](.*)[\"']", Qt::CaseInsensitive);  //get title ...->
    rx_title.setMinimal(true);
    if (-1 != (rx_title.indexIn(str, 0))) {
        attr_title = rx_title.cap(1);
    }

    //insert info to write script page
    kPointerMainWidget->ui->tagAllTextEdit->setPlainText(str);
    kPointerMainWidget->ui->tagTagNameEdit->setText(tag_name);
    kPointerMainWidget->ui->tagNameEdit->setText(attr_name);
    kPointerMainWidget->ui->tagClassEdit->setText(attr_class);
    kPointerMainWidget->ui->tagIdEdit->setText(attr_id);
    kPointerMainWidget->ui->tagTypeEdit->setText(attr_type);
    kPointerMainWidget->ui->tagSrcEdit->setText(attr_src);
    kPointerMainWidget->ui->tagHrefEdit->setText(attr_href);
    kPointerMainWidget->ui->tagValueEdit->setText(attr_value);
    kPointerMainWidget->ui->tagTitleEdit->setText(attr_title);
    kPointerMainWidget->ui->tagContentEdit->setText(attr_content);//<div class="default" id="pwd_tip3"><p>正则要匹配到此</p></div>
    //hide this widget
    this->hide();
}

void FindElement::on_showBtn_clicked()
{
    //show all html text.
}

FindElement::~FindElement()
{
    delete browserGet_;
}
