#include "StdAfx.h"
#include "conditionscript.h"

ConditionScript::ConditionScript(QObject *parent) :
    CScript(parent)
{
    this->type = CScript::XCondition;
    initData();
}

void ConditionScript::initData()
{
    browserGet_ = BrowserGet::getInstance();
}
/*网址加载成功*/
void ConditionScript::loadComplete()
{
    QObject::disconnect(browserGet_, SIGNAL(loadComplete()), this, SLOT(loadComplete()));
    browserGet_->cancelJudgeUrl();
    qDebug("load ok.........");
    judge();    /*继续判断其它选项*/
}

void ConditionScript::run()
{
    //获取各个选项的值
    QString urlsymbol = get(getopt(ConditionUrlSymbolComBox));
    QString urlvalue = get(getopt(ConditionUrlValueEdit));

    //判断条件
    if (!urlvalue.isEmpty()) { //指定了URL
        QObject::connect(browserGet_, SIGNAL(loadComplete()), this, SLOT(loadComplete()));
        browserGet_->setJudgeUrl(urlvalue, urlsymbol);
        return;
    }
    judge(); /*没有指定URL则判断其它的*/
}

void ConditionScript::stop()
{
    QObject::disconnect(browserGet_, SIGNAL(loadComplete()), this, SLOT(loadComplete()));
    browserGet_->cancelJudgeUrl();
}

void ConditionScript::judge()
{
    QString attrname = get(getopt(ConditionAttrNameEdit));
    QString attrsymbol = get(getopt(ConditionAttrSymbolComBox));
    QString attrvalue = get(getopt(ConditionAttrValueEdit));
    QString tagname = get(getopt(ConditionTagNameEdit));
    QString yes = get(getopt(ConditionYes));
    QString no = get(getopt(ConditionNo));
    QString go = get(getopt(ContinueRadioBtn));
    QString jmp = get(getopt(JmpToRadioBtn));
    int jmpto = 0;
    mark::RunDirect runstate = mark::RUN_SUCCESS;
    if (!jmp.isEmpty()) { /*使用了跳转到*/
        runstate = mark::RUN_JMP_TO;
        bool ok;            /*提取序号,发送信号时使用*/
        jmpto = QString(get(getopt(JmpIndexSpinBox))).toInt(&ok, 10);
    }
    //没指定URL则继续其它判断
    HRESULT hr;
    if (browserGet_->spWebBrowser2_) {
        CComQIPtr <IHTMLDocument2, &IID_IHTMLDocument2> spDoc2;
        CComPtr <IDispatch> spDisp;
        hr = browserGet_->spWebBrowser2_->get_Document(&spDisp);
        spDoc2 = spDisp;
        if (SUCCEEDED(hr) && spDoc2) {
            if (!tagname.isEmpty()) {
                CComQIPtr <IHTMLDocument3, &IID_IHTMLDocument3> spDoc3;
                hr = spDoc2->QueryInterface(IID_IHTMLDocument3, (void**)&spDoc3);
                if (SUCCEEDED(hr) && spDoc3) {
                    _bstr_t bsTag = tagname.toLocal8Bit().data();
                    CComQIPtr <IHTMLElementCollection, &IID_IHTMLElementCollection2> spElemCollection;
                    hr = spDoc3->getElementsByTagName(bsTag.GetBSTR(), &spElemCollection);  /*查找标签集合*/
                    if (SUCCEEDED(hr) && spElemCollection) {
                        qDebug() << "Find element collection..";
                        long count = 0;
                        hr = spElemCollection->get_length(&count);
                        for (long i = 0; i < count; ++i) {
                            CComPtr <IDispatch> spelemdisp;
                            hr = spElemCollection->item(CComVariant(i), CComVariant(), &spelemdisp);
                            if (FAILED(hr)) {
                                return;
                            }
                            CComQIPtr <IHTMLElement, &IID_IHTMLElement> spElement = spelemdisp;
                            if (!spElement) { return;   }
                            ElementInfo info(spElement);
                            QString val = info.getAttrValue(attrname);
                            if (!val.isEmpty()) {
                                if (attrsymbol == "==") {
                                    if (0 == attrvalue.compare(val, Qt::CaseInsensitive)) {
                                        if (!yes.isEmpty()) { //符合条件则
                                            emit runFinish(runstate, jmpto);
                                        }
                                    }
                                    if (!no.isEmpty()){ //不符合条件则
                                        emit runFinish(runstate, jmpto);
                                    }
                                }
                                if (attrsymbol == "!=") {
                                    if (0 != attrvalue.compare(val, Qt::CaseInsensitive)) {
                                        if (!yes.isEmpty()) { //符合条件则
                                            emit runFinish(runstate, jmpto);
                                        }
                                    }
                                    if (!no.isEmpty()){ //不符合条件则
                                        emit runFinish(mark::RUN_SUCCESS, jmpto);
                                    }
                                }
                                if (attrsymbol == cn_tr("包含")) {
                                    if (val.contains(attrvalue)) {
                                        if (!yes.isEmpty()) { //符合条件则
                                            emit runFinish(runstate, jmpto);
                                        }
                                    }
                                    if (!no.isEmpty()){ //不符合条件则
                                        emit runFinish(runstate, jmpto);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    runstate = mark::RUN_FAILED;
    emit runFinish(runstate, jmpto);
}



QString ConditionScript::getopt(const ConditionType &op)
{
    switch (op) {
    case ConditionUrlSymbolComBox: return "ConditionUrlSymbolComBox";
    case ConditionUrlValueEdit: return "ConditionUrlValueEdit";
    case ConditionAttrNameEdit: return "ConditionAttrNameEdit";
    case ConditionAttrSymbolComBox: return "ConditionAttrSymbolComBox";
    case ConditionAttrValueEdit: return "ConditionAttrValueEdit";
    case ConditionTagNameEdit: return "ConditionTagNameEdit";
    case ConditionYes: return "ConditionYes";
    case ConditionNo: return "ConditionNo";
    case ContinueRadioBtn: return "ContinueRadioBtn";
    case JmpToRadioBtn: return "JmpToRadioBtn";
    case JmpIndexSpinBox: return "JmpIndexSpinBox";
    case WaitRadioBtn: return "WaitRadioBtn";
    }
    return QString();
}

ConditionScript::~ConditionScript()
{

}


