#include "StdAfx.h"
#include "titlewidget.h"

TitleWidget::TitleWidget(QWidget *parent) :
    QWidget(parent)
{

}

void TitleWidget::mousePressEvent(QMouseEvent *event)
{
    //如果点在按钮区域  不会移动 立即返回
    /*if (event->x()+28 >= width()) {
        return;
    }*/
    pressedPoint = event->pos();
    isMove = true;
}

void TitleWidget::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons()&Qt::LeftButton) && isMove)
    {
        //由于外层还有一个水平布局，此处要获取主窗体才有用，视情况而定
        static QWidget* parWidget = parentWidget()->parentWidget();
        QPoint nowParPoint=parWidget->pos();
        nowParPoint.setX(nowParPoint.x()+event->x()-pressedPoint.x());
        nowParPoint.setY(nowParPoint.y()+event->y()-pressedPoint.y());
        parWidget->move(nowParPoint);
    }
}

void TitleWidget::mouseReleaseEvent(QMouseEvent *)
{
    if (isMove) {
        isMove = false;
    }
}


TitleWidget::~TitleWidget()
{

}
