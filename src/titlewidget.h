#ifndef TITLEWIDGET_H
#define TITLEWIDGET_H

#include <QWidget>

class QPoint;
class TitleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TitleWidget(QWidget *parent = 0);
    virtual ~TitleWidget();

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

private:
    QPoint pressedPoint;
    bool isMove;
};



#endif // TITLEWIDGET_H
