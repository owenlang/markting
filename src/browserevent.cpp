/*-----------------------------------------------------------------------------
Description: 接收处理 DIID_DWebBrowserEvents2 事件的类
Author: WRW.owenlang@163.com
Date: 2013/11/06
-----------------------------------------------------------------------------*/
#include "StdAfx.h"
#include "BrowserEvent.h"


BrowserEvent::BrowserEvent(QObject *parent) :QObject(parent)
{
    m_dwCookie = 0;
    m_pPoint = NULL;
    m_pDoc = NULL;
    m_pBrowser = NULL;
    m_pDocEvt = new HTMLDocEvent();
    useJudge_ = false;
}

HRESULT  BrowserEvent::GetTypeInfoCount(
            /* [out] */ UINT *pctinfo)
{
    pctinfo = pctinfo;//cancel the compile warning:C4100
    return E_NOTIMPL;
}

HRESULT  BrowserEvent::GetTypeInfo(
    /* [in] */ UINT iTInfo,
    /* [in] */ LCID lcid,
    /* [out] */ ITypeInfo **ppTInfo)
{
    iTInfo = iTInfo; lcid = lcid; ppTInfo = ppTInfo;
    return E_NOTIMPL;
}

HRESULT  BrowserEvent::GetIDsOfNames(
    /* [in] */ REFIID riid,
    /* [size_is][in] */ LPOLESTR *rgszNames,
    /* [in] */ UINT cNames,
    /* [in] */ LCID lcid,
    /* [size_is][out] */ DISPID *rgDispId)
{
    rgszNames = rgszNames; cNames = cNames; lcid = lcid; rgDispId = rgDispId;
    return E_NOTIMPL;
}

//安装 DIID_HTMLDocumentEvents2 事件接收器
void BrowserEvent::installDocEventsHandle()
{
	//捕获文档事件
    HRESULT hr;
    //1.判断 IWebBrowser2指针是否有效
    if (m_pBrowser) {
        //2.找到 IHTMLDocument2 指针
        CComPtr <IDispatch> pDisp;
        hr = m_pBrowser->get_Document(&pDisp);
        if (SUCCEEDED(hr)) {
            //如果还存在连接，则断开连接
            uninstallDocEventsHandle();
            //2.找到连接点容器
            CComPtr <IConnectionPointContainer> pContainer;
            hr = pDisp->QueryInterface(IID_IConnectionPointContainer, (void**)&pContainer);
            if (SUCCEEDED(hr)) {
                qDebug("Get IID_IConnectionPointContainer success.");
                //3.从连接点容器中找到 DIID_HTMLDocumentEvents 连接点
                hr = pContainer->FindConnectionPoint(DIID_HTMLDocumentEvents2, &(m_pDocEvt->m_pPoint));
                if (SUCCEEDED(hr)) {
                    qDebug("Find DIID_HTMLDocumentEvents2 connect point success.");
                    //连接到 DIID_HTMLDocumentEvents2
                    CComPtr <IUnknown> pUnk;
                    m_pDocEvt->QueryInterface(IID_IUnknown, (void**)&pUnk);
                    m_pDocEvt->m_pPoint->Advise(pUnk, &(m_pDocEvt->m_dwCookie));
                    if (SUCCEEDED(hr)) {
                        qDebug("DIID_HTMLDocumentEvents2 Advise success.");
                        m_pDocEvt->m_pDoc = pDisp;
                        return ;
                    }
                }
                qDebug("Find DIID_HTMLDocumentEvents2 connect point failed.");
            }
            qDebug("Query IID_IConnectionPointContainer failed.");
        }
        qDebug("get_Document failed.");
    }
    qDebug("IWebBrowser2 is valid.");
    return;
}
//卸载 DIID_HTMLDocumentEvents2 事件接收器
void BrowserEvent::uninstallDocEventsHandle()
{
	//如果还存在连接，则断开连接
    if (m_pDocEvt->m_dwCookie || m_pDocEvt->m_pPoint) {
        qDebug("Exist DIID_HTMLDocumentEvents2 connect point, disconnect it.");
        m_pDocEvt->m_pPoint->Unadvise(m_pDocEvt->m_dwCookie);
        m_pDocEvt->m_dwCookie = 0;
        m_pDocEvt->m_pPoint = NULL;

    }
}

HRESULT  BrowserEvent::Invoke(
    /* [in] */ DISPID dispIdMember,
    /* [in] */ REFIID riid,
    /* [in] */ LCID lcid,
    /* [in] */ WORD wFlags,
    /* [out][in] */ DISPPARAMS *pDispParams,
    /* [out] */ VARIANT *pVarResult,
    /* [out] */ EXCEPINFO *pExcepInfo,
    /* [out] */ UINT *puArgErr)
{
    lcid=lcid; wFlags=wFlags; pVarResult=pVarResult; pExcepInfo=pExcepInfo; puArgErr=puArgErr;
    USES_CONVERSION;
    if (!pDispParams) {
        return E_INVALIDARG;
    }
    switch (dispIdMember) {
    case DISPID_NEWWINDOW2:
        {
            *pDispParams->rgvarg[0].pboolVal = TRUE;
        }
        break;
    case DISPID_DOCUMENTCOMPLETE:
        {qDebug("document complete");
        installDocEventsHandle();
            if (useJudge_) {
                _bstr_t bsUrl;
                m_pBrowser->get_LocationURL(bsUrl.GetAddress());
                QString temp = QString::fromWCharArray(bsUrl.GetBSTR());
                url_.replace("http://", "");
                if ((temp.data()[temp.size()-1]) == '/') {
                    temp.remove(temp.size()-1, 1);
                }
                if (url_.data()[url_.size()-1] == '/') {
                    url_.remove(url_.size()-1, 1);
                }

                if (condition_=="==" || condition_==cn_tr("包含")) {
                    if (temp.contains(url_, Qt::CaseInsensitive)) {
                        QTimer::singleShot(2000, this, SIGNAL(loadComplete()));
                        break;
                    }
                } else if (condition_ == "!=") {
                    if (!temp.contains(url_, Qt::CaseInsensitive)) {
                        QTimer::singleShot(1000, this, SIGNAL(loadComplete()));
                        break;
                    }
                }

            } else {

            }
        }
        break;
    case DISPID_BEFORENAVIGATE2:
        qDebug() << "DISPID_BEFORENAVIGATE2";
        break;
    case DISPID_QUIT:
        qDebug() << "DISPID_QUIT";
        uninstallDocEventsHandle();
        break;
    }

    return S_OK;
}

void BrowserEvent::useJudgeComplete(const QString &url, const QString& condition)
{
    useJudge_ = true;
    url_ = url;
    condition_ = condition;

    if (m_pBrowser) {
        _bstr_t bsUrl;
        m_pBrowser->get_LocationURL(bsUrl.GetAddress());
        QString temp = QString::fromWCharArray(bsUrl.GetBSTR());
        if (temp.contains(url_, Qt::CaseInsensitive)) {
            emit loadComplete();
        }
    }
}

void BrowserEvent::cancelJudgeComplete()
{
    useJudge_ = false;
    url_ = "";
}
BrowserEvent::~BrowserEvent()
{

}
