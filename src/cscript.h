/*-----------------------------------------------------------------------------
Description: 脚本类的基类
Author: WRW owenlang@163.com
Date: 2013/11/15
-----------------------------------------------------------------------------*/
#ifndef CSCRIPT_H
#define CSCRIPT_H

#include <QObject>
#include <QMap> //for template
class QString;

class CScript : public QObject
{
    Q_OBJECT

public:
    typedef enum {RUN_NO_START, RUN_START, RUN_FINISH}RunState;

    typedef enum{
            XInvalid = 0,XStart,
                    XAccess    ,XFill       ,XKeyboard   ,XSend       ,XCondition ,
                    XVerify     ,XMouse      ,XJmp        ,XScreenshot ,
                    XDelay      ,XWindow     ,Xcollect    ,XUpload     ,
            XEnd
    }ScriptType; //脚本类型
public:
    explicit CScript(QObject *parent = 0);
    CScript(const CScript &script); //拷贝构造函数
    CScript& operator = (const CScript &script);//拷贝赋值

    //创建对象时直接指定脚本类型
    CScript(const ScriptType type);
    virtual ~CScript();
    //增加一个选项
    virtual void add(const QString option, const QString value);
    //删..
    virtual void del(const QString option);
    //查...
    virtual QString get(const QString option);
    //改..
    virtual bool set(const QString option, const QString value);
    //选项数量
    virtual int optionCount();
    //获取本条脚本类型
    virtual ScriptType getType();
    //设置本条脚本类型
    virtual void setType(const ScriptType type);
    //打印所有选项信息
    virtual void printAllInfo();
    //运行脚本
    virtual void run();
    //停止运行
    virtual void stop();
    //获取脚本运行状态
    virtual RunState getState();
    //设置脚本运行状态
    virtual void setState(RunState state);
    virtual QString getopt(const ScriptType &op);
    virtual void save();    /*保存脚本*/
    virtual void load();    /*导入脚本*/
signals:
    
public slots:
    
protected:
    ScriptType type;
    QMap <QString,QString> map;
    RunState state;
};

#endif // CSCRIPT_H
