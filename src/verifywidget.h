#ifndef VERIFYWIDGET_H
#define VERIFYWIDGET_H

#include <QWidget>

#include "ui_verifywidget.h"

class QString;
class QPixmap;

class VerifyWidget : public QWidget, public Ui::VerifyWidget
{
    Q_OBJECT
public:
    explicit VerifyWidget(QWidget *parent = 0);
    ~VerifyWidget();

    void setPicture(const QString &filename);

signals:
    void getCode(const QString &code);

public slots:
    void on_okBtn_clicked();

private:
    void initData();

    QPixmap *pixmap_;
    QString code_;
    QString tempDirName_;
};

#endif // VERIFYWIDGET_H
