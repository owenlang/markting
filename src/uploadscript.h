#ifndef UPLOADSCRIPT_H
#define UPLOADSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
QT_END_NAMESPACE

class UploadScript : public CScript
{
    Q_OBJECT
public:
    explicit UploadScript(QObject *parent = 0);
    
signals:
    
public slots:
    
};

#endif // UPLOADSCRIPT_H
