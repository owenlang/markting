#include "StdAfx.h"
#include "variablemanager.h"

VariableManager* VariableManager::self = NULL;
VariableManager* VariableManager::getInstance()
{
    if (NULL == self) {
        self = new VariableManager();
    }
    return self;
}

VariableManager::VariableManager(QWidget *parent) :
    QWidget(parent)
{
    initTable();
    createActions();
}

void VariableManager::initTable()
{
    varTable_ = new QTableWidget(this);
    QStringList header;
    header << cn_tr("") << cn_tr("") << cn_tr("");
    varTable_->setRowCount(100);
    varTable_->setColumnCount(3);
    varTable_->resize(QSize(800, 400));
//    varTable_->setEditTriggers(QAbstractItemView::NoEditTriggers);
    varTable_->setSelectionBehavior(QAbstractItemView::SelectRows);
    varTable_->setHorizontalHeaderLabels(header);

//    QTableWidgetItem *columnHeaderItem0 = varTable_->horizontalHeaderItem(0); //获得水平方向表头的Item对象
//    columnHeaderItem0->setFont(QFont("Helvetica")); //设置字体
//    columnHeaderItem0->setBackgroundColor(QColor(0,60,10)); //设置单元格背景颜色
//    columnHeaderItem0->setTextColor(QColor(200,111,30)); //设置文字颜色
}

void VariableManager::createActions()
{
    newAction_ = new QAction(cn_tr("新建"), this);
    newAction_->setIcon(QIcon(":/res/new.png"));
    newAction_->setShortcut(QKeySequence::New);
    newAction_->setStatusTip(tr("Create a new variable"));
    QObject::connect(newAction_, SIGNAL(triggered()), this, SLOT(newVar()));

    editAction_ = new QAction(cn_tr("新建"), this);
    editAction_->setIcon(QIcon(":/res/new.png"));
    editAction_->setShortcut(QKeySequence::New);
    editAction_->setStatusTip(tr("Edit a variable"));
    QObject::connect(editAction_, SIGNAL(triggered()), this, SLOT(editVar()));

    delAction_ = new QAction(cn_tr("删除"), this);
    delAction_->setIcon(QIcon(":/res/del.png"));
    delAction_->setShortcut(QKeySequence::New);
    delAction_->setStatusTip(tr("Delete a variable"));
    QObject::connect(delAction_, SIGNAL(triggered()), this, SLOT(delVar()));

    clearAction_ = new QAction(cn_tr("清空"), this);
    clearAction_->setIcon(QIcon(":/res/clear.png"));
    clearAction_->setShortcut(QKeySequence::New);
    clearAction_->setStatusTip(tr("Clear all variable"));
    QObject::connect(clearAction_, SIGNAL(triggered()), this, SLOT(clearVar()));

    this->addAction(newAction_);
    this->addAction(delAction_);
    this->addAction(clearAction_);
    this->setContextMenuPolicy(Qt::ActionsContextMenu);
}

void VariableManager::newVar()
{
    qDebug() << "new variable";
}
void VariableManager::editVar()
{
}
void VariableManager::delVar()
{

}
void VariableManager::clearVar()
{

}
QString VariableManager::getVar(const QString &var)
{

    return map_[var];
}

void VariableManager::setVar(const QString &var, const QString &value)
{
    map_[var] = value;
}

QStandardItemModel* VariableManager::getModel()
{
    return varModel_;
}

VariableManager::~VariableManager()
{
    delete varModel_;
    delete newAction_;
    delete delAction_;
    delete clearAction_;
}
