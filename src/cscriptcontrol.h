#ifndef CSCRIPTCONTROL_H
#define CSCRIPTCONTROL_H

#include <QObject>

class QTimer;

class CScriptControl : public QObject
{
    Q_OBJECT
public:
    explicit CScriptControl(QObject *parent = 0);
    virtual ~CScriptControl();

    void initData();
    void run(const int &index); //运行一条脚本
    void runGroup(); //运行一组脚本
    void addScript(CScript *script);
    void delScript(const int &index);
    void clearAllScript();
    void swapScript(const int &index1, const int &index2);
    void printAllType();

    void save(const QString& filename);
    void load(const QString& filename);

signals:
    
public slots:
    void executeTimerSlot();
    void runFinish(mark::RunDirect rd, int to);

private:
    QList <CScript*> immediateScriptList_;//使用指针要考虑资源释放问题
    QTimer *executeTimer_;
    int oldIndex_;
    int scriptIndex_;
};

#endif // CSCRIPTCONTROL_H
