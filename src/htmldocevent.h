/*-----------------------------------------------------------------------------
Description: 接收处理 DIID_HTMLDocumentEvents2 事件的类
Author: WRW owenlang@163.com
Date: 2013/11/06
-----------------------------------------------------------------------------*/
#ifndef HTMLDocEvent_H
#define HTMLDocEvent_H

#include <oaidl.h>    //包含了IDspatch接口定义
#include <Mshtml.h>    //事件IID的定义
#include <ExDisp.h>
#include <ExDispid.h>
#include <MsHtmdid.h>

#include <QObject>

QT_BEGIN_NAMESPACE
class MainWidget;
class QTimer;
QT_END_NAMESPACE

extern MainWidget *kPointerMainWidget;

class HTMLDocEvent : public QObject, public IDispatch
{
    Q_OBJECT
public:
    HTMLDocEvent(QObject *parent=0);
    virtual ~HTMLDocEvent();

    HRESULT STDMETHODCALLTYPE QueryInterface(const struct _GUID &iid, void ** ppv)
    {
        if (iid == IID_IUnknown) {
            *ppv = static_cast<IUnknown*>(this);
        } else if (iid == IID_IDispatch) {
            *ppv = static_cast<IDispatch*>(this);
        } else {
            return E_NOINTERFACE;
        }
        return S_OK;
    }

    ULONG STDMETHODCALLTYPE AddRef(void)    { return 1; }// 做个假的就可以，因为反正这个对象在程序结束前是不会退出的
    ULONG STDMETHODCALLTYPE Release(void)    { return 0; } // 做个假的就可以，因为反正这个对象在程序结束前是不会退出的
    HRESULT  STDMETHODCALLTYPE GetTypeInfoCount(/* [out] */ UINT *pctinfo);
    HRESULT  STDMETHODCALLTYPE GetTypeInfo(/* [in] */ UINT iTInfo,/* [in] */ LCID lcid,/* [out] */ ITypeInfo **ppTInfo);
    HRESULT  STDMETHODCALLTYPE GetIDsOfNames(
            /* [in] */ REFIID riid,/* [size_is][in] */ LPOLESTR *rgszNames,/* [in] */ UINT cNames,/* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);

    HRESULT  STDMETHODCALLTYPE Invoke(
            /* [in] */ DISPID dispIdMember,/* [in] */ REFIID riid,/* [in] */ LCID lcid,/* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,/* [out] */ VARIANT *pVarResult,/* [out] */ EXCEPINFO *pExcepInfo,/* [out] */ UINT *puArgErr) ;

	void uninstallDocEventsHandle();
    void updateInfo(IHTMLElement *spElem);
    void clearTagInfo();
    void beginDraw(IHTMLElement *spElem);
    void drawRestore();

signals:

public slots:
    void timeOut();

public:	
    CComPtr <IConnectionPoint> m_pPoint; //DIID_HTMLDocumentEvents2 	接收事件的连接点
    DWORD m_dwCookie;   //连接点的 cookie ,用于卸载时使用
    CComQIPtr <IHTMLDocument2> m_pDoc;
    IHTMLElement *spElement_;
    IHTMLElement *spNewElem_;
    IHTMLElement *spOldElem_;
    _bstr_t border;

    QTimer *timer_;
};

#endif // HTMLDocEvent_H
