﻿/*-----------------------------------------------------------------------------
    MainWidget.h -- MainWidget类的头文件
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#ifndef SANDBOXLABEL_H
#define SANDBOXLABEL_H

#include <QLabel>

class QMouseEvent;
class QPaintEvent;
class QWidget;

class SandboxLabel : public QLabel
{
    Q_OBJECT
public:
    explicit SandboxLabel(QWidget *parent = 0);
    ~SandboxLabel();
    
signals:
    void clicked();
    void getArea(const int& time);
    void endArea();

public slots:

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);

private:
    bool m_mousePress;
};

#endif // SANDBOX_LABEL_H
