#include "StdAfx.h"
#include "fillwebscript.h"

QString kTempCode;
FillWebScript::FillWebScript(QObject *parent) :
    CScript(parent), tempDirName_("./temp")
{
    initData();
}

void FillWebScript::initData()
{
    this->type = CScript::XFill;
    picget_ = new PictureGet();
    verifyWidget_ = new VerifyWidget();//自动隐藏与显示
    remote_ = new RemoteVerify();
    QObject::connect(remote_, SIGNAL(getCode(const QString&)), this, SLOT(getCode(const QString&)));
//    connect(picget_, SIGNAL(getPicFinished(const QString &)), this, SLOT(getPicFinished(const QString &)));
    connect(verifyWidget_, SIGNAL(getCode(const QString &)), this, SLOT(getCode(const QString &)));
}

void FillWebScript::run()
{
    //取得与浏览器有关的指针
    HRESULT hr;
    CComPtr <IHTMLDocument2> spDocument2;
    CComPtr <IDispatch> spDisp;
    browserGet_ = BrowserGet::getInstance();
    browserGet_->installWebEventsHandle();
    if (NULL == browserGet_->spWebBrowser2_) {
        qDebug("spWebBrowser2_ is NULL");
        return;
    }
    hr = browserGet_->spWebBrowser2_->get_Document(&spDisp);
    spDocument2 = spDisp;
    if (FAILED(hr)) {  qDebug("get_Document failed");
        return;
    }
    enumElement(spDocument2);
}

void FillWebScript::stop()
{

}

void FillWebScript::enumFrame(IHTMLDocument2 *pIHTMLDocument2)
{
    HRESULT hr;
    CComPtr <IHTMLFramesCollection2> spFrameCollection2;
    hr = pIHTMLDocument2->get_frames(&spFrameCollection2); //取得框架frame的集合
    if (FAILED(hr)) { qDebug("get_frames failed");
        return;
    }
    long nFrameCount = 0;               //取得子框架的个数
    hr = spFrameCollection2->get_length(&nFrameCount);
    qDebug("nFrameCount:%d", nFrameCount);
    if (FAILED(hr)) {  qDebug("get_length failed");
        return;
    }

    for (long i=0; i < nFrameCount; i++) {
        CComVariant vDispWin2;     //取得子框架的自动化接口
        hr = spFrameCollection2->item(&CComVariant(i), &vDispWin2);
        if (FAILED(hr)) continue;

        CComQIPtr <IHTMLWindow2, &IID_IHTMLWindow2> spWin2 = vDispWin2.pdispVal;
        if (!spWin2) continue;   //取得子框架的 IHTMLWindow2 接口

        CComPtr <IHTMLDocument2> spDoc2;//取得字框架的 IHTMLDocument2 接口
        spWin2->get_document(&spDoc2);
        enumElement(spDoc2);//递归枚举当前子框架 IHTMLDocument2 上的所有元素
    }
}

void FillWebScript::enumElement(IHTMLDocument2 *pIHTMLDocument2)
{
    //遍历表单,清空所有文本框时用
    enumFrame(pIHTMLDocument2);//递归枚举当前 IHTMLDocument2 上的子框架fram

    QString tagTagName = get(getopt(FillTagTagName)); //标签名
    QString tagType    = get(getopt(FillTagType)); //tagname
    QString tagClass   = get(getopt(FillTagClass)); //class
    QString tagId      = get(getopt(FillTagId)); //id
    QString tagTitle   = get(getopt(FillTagTitle)); //title
    QString tagName    = get(getopt(FillTagName)); //name
    QString tagValue   = get(getopt(FillTagValue)); //value
    QString tagSrc     = get(getopt(FillTagSrc)); //src
    QString tagHref    = get(getopt(FillTagHref)); //href
    QString tagContent = get(getopt(FillTagContent)); //content

    HRESULT hr;
    CComPtr <IHTMLElementCollection> spElementCollection; //这个集合不仅包含表单元素,也包含其它元素,如图片集合等
    hr = pIHTMLDocument2->get_all(&spElementCollection);
    if (FAILED(hr)) { qDebug("get_all failed");
        return;
    }

    long nElemCount=0;             //取得元素数目
    hr = spElementCollection->get_length(&nElemCount);
    if (FAILED(hr)) { qDebug("get_length failed");
        return;
    }
    CComQIPtr<IDispatch, &IID_IDispatch> spDisp;
    spElementCollection->tags(CComVariant(tagTagName.toLocal8Bit().data()), &spDisp); //只遍历指定的集合
    if (spDisp) {
        CComQIPtr <IHTMLElementCollection, &IID_IHTMLElementCollection> spElementColl = spDisp;
        long nElem = 0;
        spElementColl->get_length(&nElem);
        qDebug("Found %d element", nElem);
        for (int i = 0; i < nElem; ++i) {
            CComPtr <IDispatch> tempDisp;
            hr = spElementColl->item(CComVariant(i), CComVariant(), &tempDisp);
            if (FAILED(hr)) continue;
            CComQIPtr <IHTMLElement, &IID_IHTMLElement> spElement = tempDisp;

            ElementInfo element(spElement);
            //与探测到的标签信息比较,一样则执行操作
            QString tagcontent = element.getAttrValue(cn_tr("内容"));
            QString tagid = element.getAttrValue("id");
            QString tagtitle = element.getAttrValue("title");
            QString tagname = element.getAttrValue("name");
            QString tagvalue = element.getAttrValue("value");
            QString tagtype = element.getAttrValue("type");
            QString tagclass = element.getAttrValue("class");
            QString tagsrc = element.getAttrValue("src");
            QString taghref = element.getAttrValue("href");
            qDebug()<<"content:"<<tagcontent<<",id:"<<tagid<<",title:"<<tagtitle<<",href:"<<taghref;

//            HRESULT hr;
//            _bstr_t bsTag;
//            _variant_t variant;

            //6.id  //id具有唯一性,所以优先判断
            if (!tagId.isEmpty()) {qDebug("compare id");
//                hr = spElement->get_id(bsTag.GetAddress());//id
//                if (SUCCEEDED(hr)) {
//                    tagid = QString::fromWCharArray(bsTag.GetBSTR());
                    if (0 != tagId.compare(tagid, Qt::CaseInsensitive)) {
                        qDebug() << "id:" << tagId << " " << tagid;
                        qDebug("id not equal");continue;
                    } else {
                        executeScript(spElement);
                        return;
                    }
//                }
            }
            //1.比较标签名tagtagname //已经过滤掉了

            //2.比较type
            if (!tagType.isEmpty()) {qDebug("compare type");
//                CComPtr <IHTMLInputElement> spInputElement;
//                hr = spElement->QueryInterface(IID_IHTMLInputElement, (void**)&spInputElement);
//                if (SUCCEEDED(hr) && spInputElement) {
//                    hr = spInputElement->get_type(bsTag.GetAddress());
//                    if (SUCCEEDED(hr)) {
//                        tagtype = QString::fromWCharArray(bsTag.GetBSTR());
                        if (0 != tagType.compare(tagtype, Qt::CaseInsensitive)) {
                            qDebug() << "type:" << tagType << " " << tagtype;
                            qDebug("type not equal");continue;
                        }
//                    }
//                }
            }

            //3.比较class
            if (!tagClass.isEmpty()) {qDebug("compare class");
//                hr = spElement->get_className(bsTag.GetAddress()); //class
//                if (SUCCEEDED(hr)) {
//                    tagclass = QString::fromWCharArray(bsTag.GetBSTR());
                    if (0 != tagClass.compare(tagclass, Qt::CaseInsensitive)) {
                         qDebug("class not equal");continue;
                    }
//                }
            }

            //4.比较content
            if (!tagContent.isEmpty()) {qDebug("compare content");
//                hr = spElement->get_innerText(bsTag.GetAddress());//content
//                if (SUCCEEDED(hr)) {
//                    tagcontent = QString::fromWCharArray(bsTag.GetBSTR());
                    if (0 != tagContent.compare(tagcontent, Qt::CaseInsensitive)) {
                        qDebug("content not equal");continue;
                    }
//                }
            }

            //5.value
            CComPtr <IHTMLInputElement> spInputElem;
            if (!tagValue.isEmpty()) {qDebug("compare value");
//                hr = spElement->QueryInterface(IID_IHTMLInputElement, (void**)&spInputElem);
//                if (SUCCEEDED(hr) && spInputElem) {
//                    hr = spInputElem->get_value(bsTag.GetAddress());
//                    if (SUCCEEDED(hr)) {
//                        tagvalue = QString::fromWCharArray(bsTag.GetBSTR());
                        if (0 != tagValue.compare(tagvalue, Qt::CaseInsensitive)) {
                            qDebug("value not equal");continue;
                        }
//                    }
//                }
            }

            //7.name
            if (!tagName.isEmpty()) {qDebug("compare name");
//                if (spInputElem) {
//                    hr = spInputElem->get_name(bsTag.GetAddress());
//                    if (SUCCEEDED(hr)) {
//                        tagname = QString::fromWCharArray(bsTag.GetBSTR());
                        if (0 != tagName.compare(tagname, Qt::CaseInsensitive)) {
                            qDebug("name not equal");continue;
                        }
//                    }
//                }
            }

            //8.src
            if (!tagSrc.isEmpty()) {qDebug("compare src");
//                hr = spElement->getAttribute(L"src", 0, variant.GetAddress()); //src
//                if (SUCCEEDED(hr)) {
//                    tagsrc = QString::fromWCharArray(variant.bstrVal);
                    if (0 != tagSrc.compare(tagsrc, Qt::CaseInsensitive)) {
                        qDebug("src not equal");continue;
                    }
//                }
            }

            //9.href
            if (!tagHref.isEmpty()) {qDebug("compare href");
//                hr = spElement->getAttribute(L"href", 1, variant.GetAddress()); //href
//                if (SUCCEEDED(hr) && variant.vt != VT_EMPTY) {
//                    taghref = QString::fromWCharArray(variant.bstrVal);
                if ((tagHref.data()[tagHref.size()-1]) == '/') {
                    tagHref.remove(tagHref.size()-1, 1);
                }
                if (taghref.data()[taghref.size()-1] == '/') {
                    taghref.remove(taghref.size()-1, 1);
                }
                    if (0 != tagHref.compare(taghref, Qt::CaseInsensitive)) {
                        qDebug() << "href:" << tagHref << " " << taghref;
                        qDebug("href not equal");continue;
                    }
//                }
            }

            //10.title
            if (!tagTitle.isEmpty()) {qDebug("compare title");
//                hr = spElement->get_title(bsTag.GetAddress()); //title
//                if (SUCCEEDED(hr)) {
//                    tagtitle = QString::fromWCharArray(bsTag.GetBSTR());
                    if (0 != tagTitle.compare(tagtitle, Qt::CaseInsensitive)) {
                        qDebug("title not equal");continue;
                    }
//                }
            }
            //判断完都相等则找到了该标签
            qDebug("Find the element");
            executeScript(spElement);
            emit runFinish(mark::RUN_SUCCESS, 0);
            return;
        }
    }
}

void FillWebScript::executeScript(IHTMLElement *pElement)
{
    qDebug("executeScript");
    CComQIPtr <IHTMLElement, &IID_IHTMLElement> spElement = pElement;
    HRESULT hr;
    QString opVal;
    for (int op = (int)FillStart; op != FillEnd; ++op) {
        //提取各选项的值,因为每个case都需要提取,所以放到switch前
        opVal = get(getopt((FillWebOption)op));
        if (opVal=="0" || opVal.isEmpty()) { continue; } //脚本设置为有效时才执行

        //各选项的处理
        switch ((FillWebOption)op) {
        case FillClickRadio: //点击
            {
                hr = spElement->click();
                if (SUCCEEDED(hr)) {
                    qDebug("Click ok");
                    emit runFinish(mark::RUN_SUCCESS, 0);
                }
            }
            break;
        case FillSetAttrRadio: //设置属性
            {
                //获取指定的属性名FillAttrNameEdit
                QString strAttrName = get(getopt(FillAttrNameEdit));
                //获取指定的属性值FillAttrValueEdit
                QString strAttrValue = get(getopt(FillAttrValueEdit));
                //设置该标签的属性
                _bstr_t bsName;
                _variant_t varValue(strAttrValue.toLocal8Bit().data());
                for (int sleep=0; sleep<1000; sleep++) {;}
                if (strAttrName == "class") {
                    bsName = "className";//mshtml要求
                    _bstr_t bsValue(strAttrValue.toLocal8Bit().data());
                    hr = spElement->put_className(bsValue.GetBSTR());
                } else {
                    bsName = strAttrName.toLocal8Bit().data();
                    hr = spElement->setAttribute(bsName.GetBSTR(), varValue.GetVARIANT(), 1);
                }
                if (SUCCEEDED(hr)) {
                    qDebug("set attribute success");
                    emit runFinish(mark::RUN_SUCCESS, 0);
                }
            }
            break;
        case FillSetTextRadio: //设置文本
            {
                //取得文本内容,可能是随机数,多组文本等
                QString strText = get(getopt(FillSetTextEdit));
                //解析文本是否是多组定义,随机数定义等                
                QString str = strText; //必须

                QString pattern = ".*\\[verify(.*)\\]";
                QRegExp rx(pattern);
                if (rx.exactMatch(strText)) {
                    qDebug() << "pipei:" << strText << kTempCode;
                    varManager_ = VariableManager::getInstance();
                    str = varManager_->getVar("verify");
                }

                //执行设置文本操作
                _bstr_t bsText(str.toLocal8Bit().data());
                hr = spElement->put_innerText(bsText.GetBSTR());
                if (SUCCEEDED(hr)) {
                    qDebug() << "set text:" << str;
                    emit runFinish(mark::RUN_SUCCESS, 0);
                }
            }
            break;
        case FillGetVerifyCodeRadio:
            {
                QString picname = "verify.jpg";
                if (getVerifyPic(spElement, picname)) {
                    if (! QString(get(getopt(FillMannualRadioBtn))).isEmpty()) {
                        verifyWidget_->setPicture(picname); //手动输入验证码
                    }
                    if (! QString(get(getopt(FillRemoteRadioBtn))).isEmpty()) {
                        remote_->setPicture(picname); //远程输入验证码
                    }
                }

                //取得图片链接
//                QString picurl = get(getopt(FillTagHref));
//                picget_->getPic(picurl);
            }
            break;
        }
    }
}

void FillWebScript::getCode(const QString &code)
{
    if (!code.isEmpty()) {
        qDebug() << "verify code is : " << code;
        //将验证码保存到临时变量verify(用户手动选择的变量),这个变量由用户创建,用于运行后面的脚本时填入网页
        varManager_ = VariableManager::getInstance();
        varManager_->setVar("verify", code);
        emit runFinish(mark::RUN_SUCCESS, 0);
    } else {
        qDebug() << "verify code is null";
    }
}

bool FillWebScript::getVerifyPic(IHTMLElement *spElement, QString savename)
{
    //获取图片
    HRESULT hr;
    CComPtr <IHTMLElement2> spElem2;
    hr = spElement->QueryInterface(IID_IHTMLElement2, (void**)&spElem2);
    if (SUCCEEDED(hr) && spElem2) {
        CComQIPtr <IHTMLControlRange, &IID_IHTMLControlRange> spRange;
        CComPtr <IDispatch> spDisp;
        hr = spElem2->createControlRange(&spDisp);
        hr = spDisp->QueryInterface(IID_IHTMLControlRange, (void**)&spRange);
        if (SUCCEEDED(hr) && spRange) {
            CComPtr <IHTMLControlElement> spControlElem;
            hr = spElement->QueryInterface(IID_IHTMLControlElement, (void**)&spControlElem);
            if (SUCCEEDED(hr) && spControlElem) {
                hr = spRange->add(spControlElem);
                if (SUCCEEDED(hr)){
                    VARIANT_BOOL vb;
                    CComVariant vEmpty;
                    /*使用剪贴板,导致有些IE出现提示是否允许使用剪贴板,需要设置自定义安全级别
                      理想情况是先把剪贴板内容保存下来,使用完后再把内容设置回去,但剪贴板中的内容类型多样,保存方式是否可以通用?
                      */
                    hr = spRange->execCommand(CComBSTR("Copy"), VARIANT_FALSE, vEmpty, &vb);
                    if (SUCCEEDED(hr)) {
                        qDebug("copy command ok");
                        QClipboard *clipboard = QApplication::clipboard();
                        QPixmap pixmap = clipboard->pixmap(QClipboard::Clipboard);
                        QDir tempdir;
                        if (!tempdir.exists(tempDirName_)) {
                            tempdir.mkpath(tempDirName_);
                        }
                        pixmap.save(tempDirName_ + "/" + savename);
                        clipboard->clear(QClipboard::Clipboard);
                        return true;
                    }
                }
            }
        }
    }
    return false;
}
#if 0
void FillWebScript::getPicFinished(const QString &filename)
{
    qDebug() << "getPicFinished";
    if (filename.isEmpty()) {
        qDebug() << "get picture failed";
        return;
    }
    qDebug() << "get picture success,file name:" << filename;

    //方式1,自动根据图片文件名取得验证码
    VerifyCodeGet codeget;
    QString verifycode = codeget.get(filename);
    if (!verifycode.isEmpty()) {
        qDebug("get verifycode success");
    }
    //方式2,显示一个由手动输入验证码的窗口
    //将文件名传给验证码窗口,该窗口自动显示和隐藏
    verifyWidget_->setPicture(filename);
}
#endif

QString FillWebScript::getopt(const FillWebOption &op)
{
    switch (op) {
    case FillClickRadio   : return "ClickRadio";

    case FillSetAttrRadio : return "SetAttrRadio";
    case FillAttrNameEdit : return "AttrNameEdit";
    case FillAttrValueEdit: return "AttrValueEdit";

    case FillSetTextRadio : return "SetTextRadio";
    case FillSetTextEdit  : return "SetTextEdit";

    case FillGetVerifyCodeRadio : return "GetVerifyCodeRadio";
    case FillMannualRadioBtn: return "MannualRadioBtn";
    case FillRemoteRadioBtn : return "RemoteRadioBtn";

    case FillTagTagName   : return "TagTagName";
    case FillTagType      : return "TagType";
    case FillTagClass     : return "TagClass";
    case FillTagId        : return "TagId";
    case FillTagTitle     : return "TagTitle";
    case FillTagName      : return "TagName";
    case FillTagValue     : return "TagValue";
    case FillTagSrc       : return "TagSrc";
    case FillTagHref      : return "TagHref";
    case FillTagContent   : return "TagContent";

    case FillJmpCountEdit : return "JmpCountEdit";
    }
    return QString();
}

FillWebScript::~FillWebScript()
{
    delete picget_;
    delete verifyWidget_;
}
