#ifndef FILLWEBSCRIPT_H
#define FILLWEBSCRIPT_H

#include "cscript.h"
#include <QMap>

QT_BEGIN_NAMESPACE
class CScript;
class QString;
class PictureGet;
class VerifyWidget;
class RemoteVerify;
class VariableManager;
class BrowserGet;
QT_END_NAMESPACE

class FillWebScript : public CScript
{
    Q_OBJECT
public:
typedef enum {
    FillStart,      //对应的操作的选项 --开始-用于运行脚本时循环提取
    FillClickRadio,//点击

    FillSetAttrRadio,//设置属性
    FillAttrNameEdit, //属性名
    FillAttrValueEdit,//属性值

    FillSetTextRadio,//设置文本
    FillSetTextEdit,//文本编辑框

    FillGetVerifyCodeRadio,//获取验证码
    FillMannualRadioBtn,
    FillRemoteRadioBtn,

    //探测出来的信息
    FillTagTagName,
    FillTagType,
    FillTagClass,
    FillTagId,
    FillTagTitle,
    FillTagName,
    FillTagValue,
    FillTagSrc,
    FillTagHref,
    FillTagContent,

    FillJmpCountEdit,//跳过次数编辑框
    FillEnd //循环结束
}FillWebOption;

public:
    explicit FillWebScript(QObject *parent = 0);
    virtual ~FillWebScript();
    virtual void run();
    virtual void stop();
    virtual QString getopt(const FillWebOption &op);

signals:
    void runFinish(mark::RunDirect rd, int to);

public slots:
//    void getPicFinished(const QString &filename);
    void getCode(const QString &code);

private:
    void initData();
    void enumFrame(IHTMLDocument2 *pIHTMLDocument2);
    void enumElement(IHTMLDocument2 *pIHTMLDocument2);
    void executeScript(IHTMLElement *pElement);
    bool getVerifyPic(IHTMLElement *spElement, QString savename);

    PictureGet *picget_;
    VerifyWidget *verifyWidget_;
    RemoteVerify *remote_;
    VariableManager *varManager_;
    BrowserGet *browserGet_;
    QString tempDirName_;
};

#endif // FILLWEBSCRIPT_H
