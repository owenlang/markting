/*-----------------------------------------------------------------------------
    SysButton.cpp -- SysButton类的源文件
        主界面右上角的按钮
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#include "StdAfx.h"
#include "sysbutton.h"

SysButton::SysButton(QWidget *parent) :
    QPushButton(parent)
{
    btnWidth = btnHeight = 0;
    status = NORMAL;
}

void SysButton::enterEvent(QEvent *e)
{
    e = e;
    status = ENTER;
}
void SysButton::mousePressEvent(QMouseEvent *e)
{
    e = e;
    status = PRESS;
    QPushButton::mousePressEvent(e); //单击事件需要
}
void SysButton::mouseReleaseEvent(QMouseEvent *e)
{
    e = e;
    status = ENTER;
    QPushButton::mouseReleaseEvent(e); //单击事件需要
}
void SysButton::leaveEvent(QEvent *e)
{
    e = e;
    status = NORMAL;
}

void SysButton::paintEvent(QPaintEvent *e)
{
    e = e;
    if (0 == btnWidth) {
        pixmap.load(":/res/" + this->objectName() + ".png");
        if (this->objectName() == cn_tr("skinBtn")) {
            btnWidth = pixmap.width()/3; //皮肤图片只有三张
        } else {
            btnWidth = pixmap.width()/4;
        }
        btnHeight = pixmap.height();
    }
    QPainter painter(this);
    painter.drawPixmap(rect(), pixmap.copy(btnWidth*status, 0, btnWidth, btnHeight));
}

SysButton::~SysButton()
{

}
