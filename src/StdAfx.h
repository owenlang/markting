/*-----------------------------------------------------------------------------
    StdAfx.cpp -- 用于实现预编译处理的头文件
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#ifndef STDAFX_H
#define STDAFX_H

#include <QApplication>
#include <QTextCodec>
#include <QtGui>
#include <QString>
#include <QPoint>
#include <QRect>
#include <QDebug>
#include <QTimer>
#include <QTime>
#include <QEventLoop>
#include <QUrl>
#include <QSignalMapper>
#include <QList>
#include <QProcess>
#include <QStringList>
#include <QtXml>
#include <QDomDocument>
#include <QDomNode>
#include <QFile>
#include <QDomElement>
#include <QDomNodeList>
#include <QBuffer>
#include <QtGlobal>
#include <QXmlStreamWriter>
#include <QDir>
#include <QLibrary>
#include <QtWebKit/QtWebKit>
#include <QtNetwork>
#include <QDateTime>
#include <QAction>

#include <windows.h>
#include <mshtml.h>// 所有 IHTMLxxxx 的接口声明
#include <objbase.h>
#include <oleacc.h>
#include <ShlGuid.h>
#include <comdef.h>
#include <atlbase.h>
#include <Exdisp.h>
#include <ExDispid.h>
#include <atlcom.h>
#include <stddef.h>
#include <string>
#include <iostream>
//#include <tr1/memory> //shared_ptr,std::tr1
using namespace std;


#include "utils.h"
#include "GlobalConstant.h"
#include "titlewidget.h"
#include "sysbutton.h"
#include "sandboxlabel.h"
#include "mainwidget.h"
#include "cscriptcontrol.h"
#include "cwritescript.h"
#include "cscript.h"
#include "accesswebscript.h"
#include "fillwebscript.h"
#include "browserget.h"
#include "ieproxy.h"
#include "BrowserEvent.h"
#include "htmldocevent.h"
#include "pictureget.h"
#include "verifywidget.h"
#include "remoteverify.h"
#include "delayscript.h"
#include "conditionscript.h"
#include "mousemovescript.h"
#include "keyboardopscript.h"
#include "variablemanager.h"
#include "findelement.h"

#include "elementinfo.h"



#endif // STDAFX_H
