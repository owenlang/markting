#include "StdAfx.h"
#include "delayscript.h"

DelayScript::DelayScript(QObject *parent) :
    CScript(parent)
{
    initData();
}
void DelayScript::initData()
{
    this->type = CScript::XDelay;
    timer_ = new QTimer(this);
    QObject::connect(timer_, SIGNAL(timeout()), this, SLOT(timeOut()));
    /*设置随机数种子为当前时间*/
    QDateTime now; bool ok;
    seed_ = now.currentDateTime().toString("hhmmss").toInt(&ok,10);
    qsrand(seed_);
}

void DelayScript::run()
{
    long delay_time = 0;
    QString opVal;
    for (int op = (int)Start; op != End; ++op) {
        opVal = this->get(getopt((DelayOption)op));
        if (opVal=="0" || opVal.isEmpty()) { continue; }

        switch ((DelayOption)op) {
        case Delay:
            {
                bool ok;
                delay_time = opVal.toLong(&ok, 10);
                timer_->start(delay_time);
            }
            break;
        case DelayRange:
            {
                bool ok;
                QString strlow = opVal; QString strhigh = opVal;
                long low = strlow.replace(QRegExp("-.*"), "").toLong(&ok, 10);
                long high = strhigh.replace(QRegExp(".*-"), "").toLong(&ok, 10);
                delay_time = qrand()%(high-low+1) + low;
                timer_->start(delay_time);
            }
            break;
        }        
    }
}

void DelayScript::timeOut()
{
    timer_->stop();
    qDebug() << "timer stoped,emit signal";
    emit runFinish(mark::RUN_SUCCESS, 0);
}

void DelayScript::stop()
{
    timer_->stop();
    qDebug() << "timer stoped";
}

QString DelayScript::getopt(const DelayOption &option)
{
    switch (option) {
    case Delay:
        return "delay";
    case DelayRange:
        return "DelayRange";
    }
    return QString();
}

DelayScript::~DelayScript()
{
    delete timer_;
}
