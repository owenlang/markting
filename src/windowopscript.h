#ifndef WINDOWOPSCRIPT_H
#define WINDOWOPSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
QT_END_NAMESPACE

class WindowOpScript : public CScript
{
    Q_OBJECT
public:
    explicit WindowOpScript(QObject *parent = 0);
    
signals:
    
public slots:
    
};

#endif // WINDOWOPSCRIPT_H
