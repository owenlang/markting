/*-----------------------------------------------------------------------------
    utils.cpp -- 一些实用函数的源文件
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#include "StdAfx.h"

//转换成中文
const QString cn_tr(const char *str)
{
    QString cn;
    //cn = cn.fromLocal8Bit(str);//encoding is ANSI
    cn = cn.fromUtf8(str); //encoding is utf-8
    return cn;
}
//QString 转换为 char*
const char* qs_to_ch(const QString str)
{
    QByteArray byte_a = str.toLocal8Bit();//str.toLatin1();
    char *ch = byte_a.data();  //配合_bstr_t 等使用时崩溃
    return ch;
}

//QString 转换成 BSTR：
BSTR QStringToBSTR(const QString &str)
{
    return SysAllocStringLen((OLECHAR*)str.unicode(), str.length());
}

//BSTR 转换成 QString:
QString BSTRToQString(const BSTR bstr)
{
    return QString::fromWCharArray(bstr);
}

void DelayMs(int delay_time)
{
    QTime dieTime = QTime::currentTime().addMSecs(delay_time); //设置闪烁时间间隔
    while (QTime::currentTime() < dieTime) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }
}


