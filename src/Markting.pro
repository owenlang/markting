#-------------------------------------------------
#
# Project created by QtCreator 2013-10-30T10:40:36
#
#-------------------------------------------------

QT += core gui
QT += webkit
QT += network
QT += xml
QT += qaxcontainer
QT += qaxserver

TARGET = Markting
TEMPLATE = app


LIBS += -lGdi32 \
        -lUser32 \
        -lKernel32 \
        -lole32 \
        -lOleacc \
        -lShell32  #for SHGetSpecialFolderPath


CONFIG += qaxcontainer

# Use Precompiled headers (PCH)
PRECOMPILED_HEADER = StdAfx.h

SOURCES += main.cpp\
        mainwidget.cpp \
    sysbutton.cpp \
    utils.cpp \
    titlewidget.cpp \
    sandboxlabel.cpp \
    probelabel.cpp \
    browserevent.cpp \
    htmldocevent.cpp \
    cwritescript.cpp \
    cscriptcontrol.cpp \
    cscript.cpp \
    fillwebscript.cpp \
    accesswebscript.cpp \
    browserget.cpp \
    ieproxy.cpp \
    pictureget.cpp \
    remoteverify.cpp \
    delayscript.cpp \
    variablemanager.cpp \
    verifywidget.cpp \
    mousemovescript.cpp \
    keyboardopscript.cpp \
    windowopscript.cpp \
    jmpscript.cpp \
    conditionscript.cpp \
    collectscript.cpp \
    uploadscript.cpp \
    screenshotscript.cpp \
    configdeploy.cpp \
    elementinfo.cpp \
    findelement.cpp

HEADERS  += mainwidget.h \
    sysbutton.h \
    utils.h \
    titlewidget.h \
    sandboxlabel.h \
    probelabel.h \
    browserevent.h \
    htmldocevent.h \
    htmldocevent.h \
    cwritescript.h \
    cscriptcontrol.h \
    cscript.h \
    fillwebscript.h \
    accesswebscript.h \
    browserget.h \
    ieproxy.h \
    pictureget.h \
    verifywidget.h \
    remoteverify.h \
    delayscript.h \
    variablemanager.h \
    mousemovescript.h \
    keyboardopscript.h \
    windowopscript.h \
    jmpscript.h \
    conditionscript.h \
    collectscript.h \
    uploadscript.h \
    screenshotscript.h \
    configdeploy.h \
    GlobalConstant.h \
    elementinfo.h \
    findelement.h

FORMS    += mainwidget.ui \
    verifywidget.ui \
    remoteverify.ui \
    findelement.ui

RESOURCES += \
    resource.qrc



