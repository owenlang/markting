#ifndef DELAYSCRIPT_H
#define DELAYSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
class QTimer;
QT_END_NAMESPACE

class DelayScript : public CScript
{
    Q_OBJECT
public:
    typedef enum {
        Start = 0,
        Delay,       //精确延时
        DelayRange,   //范围延时
        End
    }DelayOption;
public:
    explicit DelayScript(QObject *parent = 0);
    virtual ~DelayScript();

    void initData();
    virtual void run();
    virtual void stop();
    virtual QString getopt(const DelayOption &option);
signals:
    void runFinish(mark::RunDirect rd, int to);

public slots:
    void timeOut();

private:
    QTimer *timer_;
    int seed_; //随机数种子
};

#endif // DELAYSCRIPT_H
