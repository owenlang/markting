#ifndef REMOTEVERIFY_H
#define REMOTEVERIFY_H

#include <QWidget>
#include "ui_remoteverify.h"

class QLibrary;

class RemoteVerify : public QWidget, public Ui::RemoteVerify
{
    Q_OBJECT
public:
    explicit RemoteVerify(QWidget *parent = 0);
    virtual ~RemoteVerify();
    void setPicture(const QString& filename);
signals:
    void getCode(const QString& code);

public slots:
    void on_okBtn_clicked();

private:
    typedef void (*UUWiseSoftInfoPrototype)(long softid, char *softkey);
    typedef long (*UUWiseLoginPrototype)(char  *user_name, char *password);
    typedef long (*UUWiseRecognize)(char *lpPicPath, long nCodeType, LPSTR pCodeResult);
    UUWiseSoftInfoPrototype uu_setSoftInfoA;
    UUWiseLoginPrototype uu_loginA;
    UUWiseRecognize uu_recognizeByCodeTypeAndPathA;

    void initData();
    bool login();
    void getFileMd5(LPCWSTR FileDirectory);

    QLibrary *uuwise;
    QString tempDirName_;
    bool loginState_;
};

#endif // REMOTEVERIFY_H
