#ifndef CWRITESCRIPT_H
#define CWRITESCRIPT_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QStandardItemModel;
class QStandardItem;
class QTimer;
class QString;
class CScript;
class BrowserGet;
class CScriptControl;
class RemoteVerify;
class FindElement;
QT_END_NAMESPACE

class CWriteScript : public QObject
{
    Q_OBJECT
public:
    explicit CWriteScript(QObject *parent = 0);
    virtual ~CWriteScript();

    void initData();
signals:

public slots:
    void onGetArea();//绑定窗口区域的定时器处理槽
    void endGetAreaTimer();//绑定窗口区域的定器结束
    void probeSlot();   /*探测网页*/
    void getArea(const int& time);
    //添加脚本信号的槽
    void addAccScriptSlot       ();//ok
    void addFillScriptSlot      ();//ok
    void addConditionScriptSlot ();//...
    void addVerifyScriptSlot    ();//ok
    void addMoveMouseScriptSlot ();
    void addKeyboardScriptSlot  ();
    void addWindowOpScriptSlot  ();
    void addJmpScriptSlot       ();
    void addDelayScriptSlot     ();//ok
    void addScreenshotScriptSlot();
    void addCollectScriptSlot   ();
    void addUploadScriptSlot    ();

    void runScriptGroupSlot();
    void runOneStepSlot();
    void saveScriptSlot();
    void loadScriptSlot();

    void deleteScriptSlot();
    void clearAllScriptSlot();
    void moveUpSlot();
    void moveDownSlot();
    void moveTopSlot();
    void moveBottomSlot();

    void clearTagInfoSlot();

    void remoteVerifySlot();
    void findElementSlot();
public:
    QStandardItemModel *immediateModel_; //即时显示在主界面编写脚本页面的数据模型
    QTimer *getAreatimer_; //探测窗口区域的定时器

private:
    HWND hwnd_;
    BrowserGet *browserGet_;
    CScriptControl *scriptControler_;
    RemoteVerify *remoteVerify_;
    FindElement *findElement_;
};

#endif // CWRITESCRIPT_H
