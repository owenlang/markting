#ifndef CONDITIONSCRIPT_H
#define CONDITIONSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class CScript;
class BrowserGet;
class QString;
class QTimer;
QT_END_NAMESPACE

class ConditionScript : public CScript
{
    Q_OBJECT
public:
    typedef enum{
        XStart,
            ConditionUrlSymbolComBox,
            ConditionUrlValueEdit,
            ConditionAttrNameEdit,
            ConditionAttrSymbolComBox,
            ConditionAttrValueEdit,
            ConditionTagNameEdit,
            ConditionYes,
            ConditionNo,
            ContinueRadioBtn,
            JmpToRadioBtn,
            JmpIndexSpinBox,
            WaitRadioBtn,
        XEnd
    }ConditionType;
public:
    explicit ConditionScript(QObject *parent = 0);
    ~ConditionScript();
    QString getopt(const ConditionType& type);
    virtual void run();
    virtual void stop();

signals:
    void runFinish(mark::RunDirect rd, int to);

public slots:
    void loadComplete();

private:
    void initData();
    void judge();

    BrowserGet *browserGet_;
    QTimer *timer_;
};

#endif // CONDITIONSCRIPT_H
