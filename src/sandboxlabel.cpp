﻿#include "StdAfx.h"
#include "sandboxlabel.h"

SandboxLabel::SandboxLabel(QWidget *parent) :
    QLabel(parent)
  , m_mousePress(false)
{
    //setMouseTracking(true);

    QPixmap pixmap;
    pixmap.load(":/res/drag.ico");
    setGeometry(0, 0, pixmap.width(), pixmap.height());
    setFixedHeight(pixmap.height());
    setPixmap(pixmap);
}

void SandboxLabel::mousePressEvent(QMouseEvent *event)
{
    HWND hDesk = ::GetDesktopWindow();
    ::SetCapture(hDesk);

    QCursor cur(QPixmap(":/res/eye.ico"));
    setCursor(cur);

    QPixmap pixmap;
    pixmap.load(":/res/drag2.ico");
    setPixmap(pixmap);

    if (event->button() == Qt::LeftButton) {
        m_mousePress = true;
    }

    emit getArea(300); //开始timer
}

void SandboxLabel::mouseReleaseEvent(QMouseEvent *)
{
    if (m_mousePress) {
        emit clicked();
    }
    m_mousePress = false;

    ::ReleaseCapture();
    setCursor(Qt::ArrowCursor);

    QPixmap pixmap;
    pixmap.load(":/res/drag.ico");
    setPixmap(pixmap);
    emit endArea(); //为了取消timer
}

void SandboxLabel::mouseMoveEvent(QMouseEvent *)
{
    m_mousePress = false;
}


void SandboxLabel::paintEvent(QPaintEvent *event){
    QPalette pal;
    QColor color(0, 0, 0);
    color.setAlphaF(0.1);
    pal.setBrush(backgroundRole(), color);
    setPalette(pal);
    setAutoFillBackground(true);
    QLabel::paintEvent(event);
}

SandboxLabel::~SandboxLabel()
{

}
