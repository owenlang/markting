/*-----------------------------------------------------------------------------
    MainWidget.cpp -- MainWidget类的源文件
        主界面
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
 
#include "StdAfx.h"
#include "mainwidget.h"
#include "ui_MainWidget.h"

MainWidget* MainWidget::single = NULL;
MainWidget* MainWidget::get_instance()
{
    if (NULL == single) {
        single = new MainWidget();
    }
    return single;
}

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    initStyle();         //窗体的初始化设置
    initSignalMapper();  //切换页面的按钮,单击信号重映射
    initData();          //一些信号和数据初始化
}

void MainWidget::initStyle()
{
    //去掉标题栏
    setWindowFlags(Qt::FramelessWindowHint);
    //背景图片
    backPicName = ":/res/skin/blueSky.jpg";
    //移动窗口到屏幕右边
    QDesktopWidget *desktop = QApplication::desktop();
    int width = desktop->screen()->width();
    int height = desktop->screen()->height();
    this->move(width/2, height/5);
}

void MainWidget::initData()
{
    //主界面右上角关闭与最小化按钮的信号
    QObject::connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
    QObject::connect(ui->miniBtn, SIGNAL(clicked()), this, SLOT(showMinimized()));

    //信号映射会发送多次信号，打开IE浏览器单独处理
    QObject::connect(ui->ieBrowserBtn, SIGNAL(clicked()), this, SLOT(openIESlot()));

    writeScript_ = new CWriteScript(this);
    //即时显示的脚本动作作表
    ui->immScriptTable->setModel(writeScript_->immediateModel_); //将数据模型与QTableView绑定
    ui->immScriptTable->setColumnWidth(0, 60); //设置列宽要放在后面，否则失效
    ui->immScriptTable->setColumnWidth(1, 290);
//    ui->immScriptTable->setColumnCount(2);
//    QStringList header;
//    header << cn_tr("类型") << cn_tr("动作");
//    ui->immScriptTable->setHorizontalHeaderLabels(header);
//    ui->immScriptTable->insertRow(ui->immScriptTable->rowCount());
//    ui->immScriptTable->setItem(0, 0, new QTableWidgetItem(cn_tr("夺得")));
//    ui->immScriptTable->insertRow(ui->immScriptTable->rowCount());
//    ui->immScriptTable->setItem(1, 0, new QTableWidgetItem(cn_tr("夺得")));

    //变量管理
    varManager_ = new VariableManager();
    ui->tabWidget->addTab(varManager_, cn_tr(""));
    QObject::connect(ui->addAccessBtn     , SIGNAL(clicked()), writeScript_, SLOT(addAccScriptSlot()));
    QObject::connect(ui->addFillWebBtn    , SIGNAL(clicked()), writeScript_, SLOT(addFillScriptSlot()));
    QObject::connect(ui->addConditionBtn  , SIGNAL(clicked()), writeScript_, SLOT(addConditionScriptSlot()));
    QObject::connect(ui->addVerifyBtn     , SIGNAL(clicked()), writeScript_, SLOT(addVerifyScriptSlot()));
    QObject::connect(ui->addMoveMouseBtn  , SIGNAL(clicked()), writeScript_, SLOT(addMoveMouseScriptSlot()));
    QObject::connect(ui->addKeyboardBtn   , SIGNAL(clicked()), writeScript_, SLOT(addKeyboardScriptSlot()));
    QObject::connect(ui->addWindowOpBtn   , SIGNAL(clicked()), writeScript_, SLOT(addWindowOpScriptSlot()));
    QObject::connect(ui->addJmpBtn        , SIGNAL(clicked()), writeScript_, SLOT(addJmpScriptSlot()));
    QObject::connect(ui->addDelayBtn      , SIGNAL(clicked()), writeScript_, SLOT(addDelayScriptSlot()));
    QObject::connect(ui->addScreenshotBtn , SIGNAL(clicked()), writeScript_, SLOT(addScreenshotScriptSlot()));
    QObject::connect(ui->addCollectBtn    , SIGNAL(clicked()), writeScript_, SLOT(addCollectScriptSlot()));
    QObject::connect(ui->addUploadBtn     , SIGNAL(clicked()), writeScript_, SLOT(addUploadScriptSlot()));

    //远程代打窗口
    QObject::connect(ui->remoteVerifyBtn, SIGNAL(clicked()), writeScript_, SLOT(remoteVerifySlot()));

    //运行脚本信号
    QObject::connect(ui->runScriptGroupBtn, SIGNAL(clicked()), writeScript_, SLOT(runScriptGroupSlot()));
    QObject::connect(ui->runOneStepBtn, SIGNAL(clicked()), writeScript_, SLOT(runOneStepSlot()));
    /*保存导入脚本*/
    QObject::connect(ui->saveScriptBtn, SIGNAL(clicked()), writeScript_, SLOT(saveScriptSlot()));
    QObject::connect(ui->loadScriptBtn, SIGNAL(clicked()), writeScript_, SLOT(loadScriptSlot()));
    //对脚本列表的操作
    QObject::connect(ui->deleteScriptBtn, SIGNAL(clicked()), writeScript_, SLOT(deleteScriptSlot()));
    QObject::connect(ui->clearAllScriptBtn, SIGNAL(clicked()), writeScript_, SLOT(clearAllScriptSlot()));
    QObject::connect(ui->moveUpBtn, SIGNAL(clicked()), writeScript_, SLOT(moveUpSlot()));
    QObject::connect(ui->moveDownBtn, SIGNAL(clicked()), writeScript_, SLOT(moveDownSlot()));
    QObject::connect(ui->moveTopBtn, SIGNAL(clicked()), writeScript_, SLOT(moveTopSlot()));
    QObject::connect(ui->moveBottomBtn, SIGNAL(clicked()), writeScript_, SLOT(moveBottomSlot()));
    //绑定窗口
    QObject::connect(ui->sandboxLabel, SIGNAL(getArea(const int&)), writeScript_, SLOT(getArea(const int&)));
    QObject::connect(ui->sandboxLabel, SIGNAL(endArea()), writeScript_, SLOT(endGetAreaTimer()));
    //探测网页
    QObject::connect(ui->probeBtn, SIGNAL(clicked()), writeScript_, SLOT(probeSlot()));
    //find element
    QObject::connect(ui->enumProbeBtn, SIGNAL(clicked()), writeScript_, SLOT(findElementSlot()));
    //清除所有探测出来的标签信息
    QObject::connect(ui->clearTagInfoBtn, SIGNAL(clicked()), writeScript_, SLOT(clearTagInfoSlot()));
    //初始化COM库
    CoInitialize(NULL);
}

void MainWidget::initSignalMapper()
{
    //主工具按钮的点击信号切换客户区
    QPushButton* toolButtons[3] = {ui->writeScriptBtn,
                                   ui->scriptManagerBtn,
                                   ui->textManagerBtn
                                  };
    toolBtnSignalMapper = new QSignalMapper(this);
    QObject::connect(toolBtnSignalMapper, SIGNAL(mapped(const int &)),
                     this, SIGNAL(toolBtnClicked(const int &)));
    for (int i = 0; i < sizeof(toolButtons) / sizeof(QPushButton*); ++i) {
        QObject::connect(toolButtons[i], SIGNAL(clicked()), toolBtnSignalMapper, SLOT(map()));
        toolBtnSignalMapper->setMapping(toolButtons[i], i);
        QObject::connect(this, SIGNAL(toolBtnClicked(const int &)), this, SLOT(mainPageChangeSlot(const int &)));
    }

    //编写脚本页面的各个按钮信号处理
    QPushButton *writePageButtons[12] = {ui->accWebBtn,
                                        ui->webFillBtn,
                                        ui->conditionBtn,
                                        ui->verifyCodeBtn,
                                        ui->mouseMoveBtn,
                                        ui->keyboardOpBtn,
                                        ui->windowOpBtn,
                                        ui->scriptJmpBtn,
                                        ui->delayBtn,
                                        ui->screenshotBtn,
                                        ui->collectorBtn,
                                        ui->uploadBtn
                                        };
    writePageSignalMapper = new QSignalMapper(this);
    QObject::connect(writePageSignalMapper, SIGNAL(mapped(const int &)),
                     this, SIGNAL(writePageBtnClicked(const int &)));
    for (int i = 0; i < sizeof(writePageButtons) / sizeof(QPushButton*); ++i) {
        QObject::connect(writePageButtons[i], SIGNAL(clicked()), writePageSignalMapper, SLOT(map()));
        writePageSignalMapper->setMapping(writePageButtons[i], i);
        QObject::connect(this, SIGNAL(writePageBtnClicked(const int &)), this, SLOT(writePageChangeSlot(const int &)));
    }
}

////打开IE浏览器,不要使用硬编码方式
void MainWidget::openIESlot()
{    
#if 1
    HKEY hkRoot,hSubKey; //定义注册表根关键字及子关键字
    char ValueName[256];
    unsigned char dataValue[256];
    unsigned long cbValueName=256;
    unsigned long cbDataValue=256;
//    char shellChar[256]; //定义命令行
    DWORD dwType;

    QString url = "http://mail.163.com/";
    QString path;
    //打开注册表根关键字
    if (RegOpenKey(HKEY_CLASSES_ROOT,NULL,&hkRoot) == ERROR_SUCCESS) {
        //打开子关键字
        if(RegOpenKeyExA(hkRoot, "htmlfile\\shell\\open\\command",
                         0, KEY_ALL_ACCESS, &hSubKey)==ERROR_SUCCESS) {
            //读取注册表，获取默认浏览器的命令行
            RegEnumValueA(hSubKey,  0, ValueName, &cbValueName, NULL,&dwType,dataValue, &cbDataValue);
            // 调用参数（主页地址）赋值
//            strcpy(shellChar,(char *)dataValue);
//            path = shellChar;
            path = (char*)dataValue;
            if (!path.contains("iexplore.exe")) {
                if(RegOpenKeyExA(hkRoot, "htmlfile\\shell\\opennew\\command",
                                 0, KEY_ALL_ACCESS, &hSubKey)==ERROR_SUCCESS) {
                    //读取注册表，获取默认浏览器的命令行
                    RegEnumValueA(hSubKey,  0, ValueName, &cbValueName, NULL,&dwType,dataValue, &cbDataValue);
                    // 调用参数（主页地址）赋值
                    //strcpy(shellChar,(char *)dataValue);
                    path = (char*)dataValue;                    
                }
            }

            if (path.contains("iexplore.exe")) {
                path.replace("%1", url);
                // 启动浏览器
                WinExec(path.toLocal8Bit().data(),SW_SHOW);
            } else {
                qWarning("找不到IE浏览器!");
            }
        }
        //关闭注册表
        RegCloseKey(hSubKey);
        RegCloseKey(hkRoot);
    }
#else
        QString program = "C:/Program Files/Internet Explorer/iexplore.exe";
        QStringList arguments;
        QProcess *myProcess = new QProcess();
        arguments << "http://zc.qq.com/chs/index.html";
        myProcess->start(program, arguments);
#endif
}

void MainWidget::mainPageChangeSlot(const int &index)
{    
    ui->stackedWidget->setCurrentIndex(index);
}

void MainWidget::writePageChangeSlot(const int &index)
{
    ui->writeStackedWidget->setCurrentIndex(index);
}

void MainWidget::on_setTextBox_currentIndexChanged(const QString &text)
{
    ui->fillSetTextEdit->append("[" + text + "]");
}

//void MainWidget::on_conditionNameComBox_currentIndexChanged(const QString &text)
//{
//    if (0 == text.compare(cn_tr("网址"), Qt::CaseInsensitive)) {
//        ui->conditionNameEdit->setText("url");
//    }
//    if (0 == text.compare(cn_tr("属性"), Qt::CaseInsensitive)) {
//        ui->conditionNameEdit->setText("id");
//    }
//}

//void MainWidget::on_conditionNameEdit_textChanged ( const QString & text )
//{
//    if (0 == text.trimmed().compare(cn_tr("url"), Qt::CaseInsensitive)) {
//        ui->conditionNameComBox->setCurrentIndex(0);
//    }
//    if (0 == text.trimmed().compare(cn_tr("tag"), Qt::CaseInsensitive)) {
//        ui->conditionNameComBox->setCurrentIndex(1);
//    }
//    ui->conditionNameComBox->setCurrentIndex(2);
//}

void MainWidget::paintEvent(QPaintEvent *e)
{
    e = e;
    QBitmap bitmap(size());
    bitmap.fill();
    QPainter painter(&bitmap);
    painter.setBrush(QBrush(Qt::black));
    painter.drawRoundedRect(bitmap.rect(), 3, 3);
    setMask(bitmap);

    QPixmap pixmap(size());
    pixmap.fill();
    painter.end();
    painter.begin(&pixmap);
    painter.setRenderHints(QPainter::Antialiasing, true);
    QBrush brush;
    brush.setTextureImage(QImage(backPicName));
    painter.setBrush(brush);
    painter.setPen(Qt::black);
    painter.drawRoundedRect(rect(), 3, 3);
    painter.end();
    painter.begin(this);
    painter.drawPixmap(rect(), pixmap);
    /*
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);*/
}

void MainWidget::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape) {
        qApp->quit();
    }
    if (e->key() == Qt::Key_F5) {
        qDebug("This is test func:");
        func_test();
    }
//    QWidget *widget = QApplication::focusWidget();
//    if (ui->keyEdit->objectName() == widget->objectName()) {
//        qDebug() << "get focus...";
//    }
}

MainWidget::~MainWidget()
{
    CoUninitialize();
    delete ui;
    delete toolBtnSignalMapper;
    delete writePageSignalMapper;
    delete writeScript_;
}
void MainWidget::func_test()
{

}
