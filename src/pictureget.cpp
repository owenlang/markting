#include "StdAfx.h"
#include <QtWebKit/QtWebKit>
#include <QtNetwork>
#include <QDateTime>
#include "pictureget.h"

#include <Shlobj.h>

PictureGet::PictureGet(QObject *parent) :
    QObject(parent), dirName_("./temp")
{
    initData();
}

void PictureGet::getPic(const QString &url)
{
    nam_->get(QNetworkRequest(QUrl(url)));//从网址下载
    //从IE临时目录获取
    QString str_path;
    wchar_t path[MAX_PATH];
    if (SHGetSpecialFolderPath(NULL, path, CSIDL_INTERNET_CACHE, FALSE)) { //取得IE临时目录路径
         str_path = QString::fromWCharArray(path);
    }
    /*https://passport.csdn.net/ajax/verifyhandler.ashx?r_d=70181*/
    qDebug() << str_path;
    //从url中提取文件名
    QString filename = url;
    QRegExp exp(".*/");
    filename.replace(exp, "");
    qDebug() << "filename:" << filename;
}

void PictureGet::setPic(const QString &)
{

}

void PictureGet::replyFinished(QNetworkReply *reply)
{
    qDebug("reply finished");
    if (reply->error() == QNetworkReply::NoError) {
        pixmap_->loadFromData(reply->readAll());
        QDateTime nowtime;
        QString filename = nowtime.currentDateTime().toString("yyyyMMddhhmmss.jpg");

        if (!dir_->exists(dirName_)) {
            qDebug("picture directory not exist, so build it");
            dir_->mkpath(dirName_);
        }

        QString savepath = dirName_ +"/"+ filename;
        if (pixmap_->save(savepath)) {
            qDebug() << "picture saved as" << savepath;
            emit getPicFinished(filename);
        }
    } else {
        qDebug("reply error");
    }
}

void PictureGet::initData()
{
    nam_ = new QNetworkAccessManager();
    pixmap_ = new QPixmap();
    retryCounter_ = 3;
    dir_ = new QDir();
    connect(nam_, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
}


PictureGet::~PictureGet()
{
    delete pixmap_;
    delete nam_;
    delete dir_;
}
