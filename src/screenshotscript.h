#ifndef SCREENSHOTSCRIPT_H
#define SCREENSHOTSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
QT_END_NAMESPACE

class ScreenshotScript : public CScript
{
    Q_OBJECT
public:
    explicit ScreenshotScript(QObject *parent = 0);
    
signals:
    
public slots:
    
};

#endif // SCREENSHOTSCRIPT_H
