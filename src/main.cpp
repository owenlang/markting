/*-----------------------------------------------------------------------------
    main.cpp -- main函数所在的源文件
        主程序
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#include "StdAfx.h"
#include <QtGui/QApplication>
#include "mainwidget.h"



MainWidget *kPointerMainWidget; //在其它地方要使用kPointerMainWidget 需要添加 ui_mainwidget.h头文件,否则找不到ui指针
mark::ProbeState kProbeState = mark::NO_PROBE;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWidget *w = MainWidget::get_instance();
    kPointerMainWidget = w;

    w->show();

    return a.exec();
}
