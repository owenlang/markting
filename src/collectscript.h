#ifndef COLLECTSCRIPT_H
#define COLLECTSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
QT_END_NAMESPACE

class CollectScript : public CScript
{
    Q_OBJECT
public:
    explicit CollectScript(QObject *parent = 0);
    
signals:
    
public slots:
    
};

#endif // COLLECTSCRIPT_H
