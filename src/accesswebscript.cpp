#include "StdAfx.h"
#include "AccessWebScript.h"

AccessWebScript::AccessWebScript(QObject *parent) :
    CScript(parent)
{
    this->type = CScript::XAccess;

    browserGet_ = BrowserGet::getInstance();
}

void AccessWebScript::run()
//运行脚本,注意选项的执行顺序问题
//如果有url,则访问前可以设置代理,禁用弹窗,访问网页后可以执行清空cookies
//如果无url,则可以执行清空网页所有文本框内容,刷新网页,清空cookies
//总结:无论有无设置url,访问前可以使用 清空网页所有文本框内容,清空当前光标处内容,刷新网页,清空cookies,设置代理,禁用弹窗
{
    //取得与浏览器有关的指针
    HRESULT hr;
    CComPtr <IHTMLDocument2> spDocument2;
    CComPtr <IDispatch> spDisp;
    if (NULL == browserGet_->spWebBrowser2_) {
        return;
    }
    hr = browserGet_->spWebBrowser2_->get_Document(&spDisp);
    spDocument2 = spDisp;
    if (FAILED(hr)) {  qDebug("get_Document failed");
        return;
    }
    //根据各选项执行不同动作
    bool need_open_url = false; //选项中有网址时才会打开
    QString opVal; //选项值
    for (int op = (int)AccStart; op != (int)AccEnd; ++op) {
        //提取各选项的值,因为每个case都需要提取,所以放到switch前
        if (get(getopt((AccessWebOption)op)).isEmpty()) { continue; }
            //各选项的处理
            switch ((AccessWebOption)op) {
            case AccUrl:
                if (opVal != "http://") {
                    need_open_url = true;
                }
                break;
            case AccClearAllText:
                enumForm(spDocument2);
                break;            
            case AccClearCookies:
                {
                    BSTR bstrCookie;
                    hr = spDocument2->get_cookie(&bstrCookie);
                    if (SUCCEEDED(hr)) {
                        qDebug() << "Cookie:" << QString::fromWCharArray(bstrCookie);
                    }
                    //清空cookies
                    hr = spDocument2->put_cookie(L"");  //无效???
                    if (SUCCEEDED(hr)) {
                        qDebug("Clear cookies success");
                    } else {
                        qDebug("Clear cookies failed");
                    }
                }
                break;
            case AccDisablePopup:
                {
                    //置标志,在DISP_NEWWINDOW2 中作个判断即可
                }
                break;
            case AccUseProxy:
                {
                    //获取代理
                    //设置代理
                }
                break;
            case AccRefresh:
                {
                    _variant_t var(REFRESH_COMPLETELY);
                    browserGet_->spWebBrowser2_->Refresh2(var.GetAddress());

                    break;
                }
            }
    } //end for

    if (need_open_url) {
        need_open_url = false;
        opVal = get(getopt(AccUrl));
        if (!opVal.isEmpty()) {            
            //用绑定的浏览器打开网页
            qDebug("ready to open %s", opVal.toLocal8Bit().data());
            browserGet_->openUrl(opVal);
            QObject::connect(browserGet_, SIGNAL(loadComplete()), this, SLOT(loadComplete()));
            browserGet_->setJudgeUrl(opVal, "==");
        }
    }
}

void AccessWebScript::stop()
{
    browserGet_->spWebBrowser2_->Stop();
    browserGet_->cancelJudgeUrl();
}

void AccessWebScript::loadComplete()
{
    QObject::disconnect(browserGet_, SIGNAL(loadComplete()), this, SLOT(loadComplete()));
    browserGet_->cancelJudgeUrl();
    emit runFinish(mark::RUN_SUCCESS, 0);
}

void AccessWebScript::enumFrame(IHTMLDocument2 *pIHTMLDocument2)
//遍历子框架
{
    HRESULT hr;
    CComPtr <IHTMLFramesCollection2> spFrameCollection2;
    hr = pIHTMLDocument2->get_frames(&spFrameCollection2); //取得框架frame的集合
    if (FAILED(hr)) { qDebug("get_frames failed");
        return;
    }
    long nFrameCount = 0;               //取得子框架的个数
    hr = spFrameCollection2->get_length(&nFrameCount);
    qDebug("nFrameCount:%d", nFrameCount);
    if (FAILED(hr)) {  qDebug("get_length failed");
        return;
    }

    for (long i=0; i < nFrameCount; i++) {
        CComVariant vDispWin2;     //取得子框架的自动化接口
        hr = spFrameCollection2->item(&CComVariant(i), &vDispWin2);
        if (FAILED(hr)) continue;

        CComQIPtr <IHTMLWindow2, &IID_IHTMLWindow2> spWin2 = vDispWin2.pdispVal;
        if (!spWin2) continue;   //取得子框架的 IHTMLWindow2 接口

        CComPtr <IHTMLDocument2> spDoc2;//取得字框架的 IHTMLDocument2 接口
        spWin2->get_document(&spDoc2);
        enumForm(spDoc2);//递归枚举当前子框架 IHTMLDocument2 上的表单form
    }
}

void AccessWebScript::enumForm(IHTMLDocument2 *pIHTMLDocument2)
//遍历表单,清空所有文本框时用
{
    enumFrame(pIHTMLDocument2);//递归枚举当前 IHTMLDocument2 上的子框架fram

    HRESULT hr;
    CComPtr <IHTMLElementCollection> spElementCollection; //这个集合不仅包含表单元素,也包含其它元素,如图片集合等
    hr = pIHTMLDocument2->get_forms(&spElementCollection);
    if (FAILED(hr)) { qDebug("get_forms failed");
        return;
    }

    long nFormCount;             //取得表单数目
    hr = spElementCollection->get_length(&nFormCount);
    qDebug("nFormCount:%d", nFormCount);
    if (FAILED(hr)) { qDebug("get_length failed");
        return;
    }

    for (int i=0; i < nFormCount; i++) {
        IDispatch *pElemDisp = NULL; //取得第 i 项表单
        hr = spElementCollection->item(CComVariant(i), CComVariant(), &pElemDisp);
        if (FAILED(hr)) continue;

        CComQIPtr <IHTMLFormElement, &IID_IHTMLFormElement> spFormElement = pElemDisp;
        pElemDisp->Release();///防止内存泄露

        long nElemCount = 0;        //取得表单中 域 的数目
        hr = spFormElement->get_length(&nElemCount);
        if (FAILED(hr)) continue;

        for (long j=0; j < nElemCount; j++) {
            CComPtr <IDispatch> pInputElemDisp;
            CComPtr <IHTMLInputTextElement> spInputTextElement;
            hr = spFormElement->item(_variant_t(j), _variant_t(), &pInputElemDisp);
            if (FAILED(hr)) continue;
            hr = pInputElemDisp->QueryInterface(IID_IHTMLInputTextElement, (void**)&spInputTextElement);
            if (FAILED(hr))  continue;
            _bstr_t bsType, bsName, bsVal;
            if (FAILED( spInputTextElement->get_type(bsType.GetAddress()) )) continue;
            if (FAILED( spInputTextElement->get_name(bsName.GetAddress()) )) continue;
            if (FAILED( spInputTextElement->get_value(bsVal.GetAddress()) )) continue;
            QString strType(QString::fromWCharArray(bsType.GetBSTR()));
            QString strName(QString::fromWCharArray(bsName.GetBSTR()));
            QString strVal(QString::fromWCharArray(bsVal.GetBSTR()));

            qDebug() << "[" << strType << "]" << strName << "=" << strVal;
            if ((strType=="text" || strType=="password") && !strName.isEmpty()) {  //清空
                spInputTextElement->put_value(L"");
            }
        }
    }
}


QString AccessWebScript::getopt(const AccessWebOption &op)
{
    switch (op) {
       case AccUrl: return "url";
       case AccClearAllText: return "ClearAllText";
       case AccClearCookies: return "ClearCookies";
       case AccDisablePopup: return "DisablePopup";
       case AccUseProxy: return "UseProxy";
       case AccRefresh: return "Refresh";
    }
    return QString();
}

void AccessWebScript::save()
{
    for (int i = (int)AccStart; i < (int)AccEnd; ++i) {
        QString value = get(getopt((AccessWebOption)i));    /*取值,有才继续保存*/
        if (value.isEmpty()) continue;
        QString option = getopt((AccessWebOption)i);   /*取出option本身的字符串*/
        QDomElement element;
        element.setTagName(option);
        qDebug() << " " << option;
    }
}
void AccessWebScript::load()
{

}

AccessWebScript::~AccessWebScript()
{
}
