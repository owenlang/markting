#ifndef PICTUREGET_H
#define PICTUREGET_H

#include <QObject>

class QUrl;
class QNetworkAccessManager;
class QNetworkReply;
class QWebView;
class QPixmap;
class QPaintEvent;
class QString;
class QDir;

class PictureGet : public QObject
{
    Q_OBJECT
public:
    explicit PictureGet(QObject *parent = 0);
    ~PictureGet();
    void getPic(const QString& url);
    void setPic(const QString& filename);

signals:
    void getPicFinished(const QString &filename);
    
public slots:
    void replyFinished(QNetworkReply *reply);

private:
    void initData();

    QNetworkAccessManager *nam_;
    QPixmap *pixmap_;
    int retryCounter_;

    QDir *dir_;
    QString dirName_;
};

#endif // PICTUREGET_H
