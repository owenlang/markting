#include "StdAfx.h"
#include "verifywidget.h"
#include "ui_verifywidget.h"

VerifyWidget::VerifyWidget(QWidget *parent) :
    QWidget(parent), tempDirName_("./temp")
{
    setupUi(this);
    initData();
}
void VerifyWidget::initData()
{
    pixmap_ = new QPixmap();
    this->hide();
}

void VerifyWidget::setPicture(const QString &filename)
{    
    //绘制验证码图片
    pixmap_->load(tempDirName_ + "/" + filename);
    picLabel->setPixmap(*pixmap_);
    this->show();
}


void VerifyWidget::on_okBtn_clicked()
{
    //点击后保存验证码
    code_ = verifyCodeEdit->text().trimmed();
    //隐藏窗口
    this->hide();
    emit getCode(code_);
}

VerifyWidget::~VerifyWidget()
{
    delete pixmap_;
}
