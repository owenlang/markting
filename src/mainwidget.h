/*-----------------------------------------------------------------------------
    MainWidget.h -- MainWidget类的头文件
        主界面
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

#include <ExDisp.h>//for include CComPtr and IWebBrowser2
#include <atlbase.h>
#include <QList> //for template class

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWidget;
}
class QStandardItemModel;
class QStandardItem;
class QSignalMapper;
class QAxObject;
class QSharedMemory;
class BrowserEvent;
class CScript;
class CWriteScript;
class VariableManager;
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit MainWidget(QWidget *parent = 0);
    static MainWidget* get_instance();
    virtual ~MainWidget();
    void initStyle();
    void initSignalMapper();
    void initData();
	void func_test();
signals:
    //主界面的button
    void toolBtnClicked(const int &index);
    //编写脚本页面中的button
    void writePageBtnClicked(const int &index);

public slots:
    void mainPageChangeSlot(const int &index);
    void openIESlot();
    void writePageChangeSlot(const int &index);

    void on_setTextBox_currentIndexChanged ( const QString & text);
//    void on_conditionNameComBox_currentIndexChanged ( const QString & text );
//    void on_conditionNameEdit_textChanged( const QString & text);

protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *);

public:
    Ui::MainWidget *ui;//为了在其它类中也能访问ui,故为public

private:
    static MainWidget *single;
    QString backPicName;
    QSignalMapper *toolBtnSignalMapper;
    QSignalMapper *writePageSignalMapper;
    CWriteScript *writeScript_;
    VariableManager *varManager_;
};

#endif // MAINWIDGET_H
