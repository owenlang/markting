#ifndef ElementInfo_H
#define ElementInfo_H

#include <QObject>

class ElementInfo : public QObject
{
    Q_OBJECT
public:
    explicit ElementInfo(IHTMLElement *pelement, QObject *parent = 0);
    virtual ~ElementInfo();

    QString getAttrValue(const QString& attr);
    void setAttrValue(const QString& attr, const QString& value);
signals:
    
public slots:

private:
    void initData();
    QString getAttrElement(const QString& attr); /*element*/
    QString getAttrAnchor(const QString& attr); /*a*/
    QString getAttrArea(const QString& attr);   /*area*/
    QString getAttrBody(const QString& attr);   /*body*/
    QString getAttrBr(const QString& attr);   /*br*/
    QString getAttrButton(const QString& attr);   /*button*/
    QString getAttrDd(const QString& attr);   /*dd*/
    QString getAttrWindow(const QString& attr);   /*window*/
    QString getAttrDiv(const QString& attr);   /*div*/
    QString getAttrDl(const QString& attr);   /*dl*/
    QString getAttrDocument(const QString& attr);   /*document*/
    QString getAttrDt(const QString& attr);   /*dt*/
    QString getAttrFont(const QString& attr);   /*font*/

    QString getAttrForm(const QString& attr);   /*form*/
    QString getAttrFrame(const QString& attr);   /*frame*/
    QString getAttrXml(const QString& attr);   /*xml*/
    QString getAttrHead(const QString& attr);   /*head*/
    QString getAttrH1H6(const QString& attr);   /*h1 through h6*/
    QString getAttrHr(const QString& attr);   /*hr*/
    QString getAttrHtml(const QString& attr);   /*html element*/
    QString getAttrIframe(const QString& attr);   /*iframe*/
    QString getAttrImg(const QString& attr);   /*img*/
    QString getAttrInput(const QString& attr);   /*input*/
    QString getAttrLabel(const QString& attr);   /*label*/
    QString getAttrLegend(const QString& attr);   /*legend*/

    QString getAttrLi(const QString& attr);   /*li*/
    QString getAttrLink(const QString& attr);   /*link*/
    QString getAttrMenu(const QString& attr);   /*menu*/
    QString getAttrLocation(const QString& attr);   /*location*/
    QString getAttrMap(const QString& attr);   /*map*/
    QString getAttrMarquee(const QString& attr);   /*marquee*/
    QString getAttrMeta(const QString& attr);   /*meta*/
    QString getAttrObject(const QString& attr);   /*object*/
    QString getAttrOl(const QString& attr);   /*ol*/
    QString getAttrUserProfile(const QString& attr);   /*userProfile*/
    QString getAttrOption(const QString& attr);   /*option*/
    QString getAttrP(const QString& attr);   /*p*/

    QString getAttrIns(const QString& attr);   /*ins*/
    QString getAttrDel(const QString& attr);   /*del*/
    QString getAttrEmbed(const QString& attr);   /*embed*/
    QString getAttrPopup(const QString& attr);   /*popup*/
    QString getAttrScreen(const QString& attr);   /*screen*/
    QString getAttrScript(const QString& attr);   /*script*/
    QString getAttrSelect(const QString& attr);   /*select*/
    QString getAttrSpan(const QString& attr);   /*span*/

    QString getAttrPage(const QString& attr);   /*page*/
    QString getAttrTable(const QString& attr);   /*table*/
    QString getAttrCaption(const QString& attr);   /*caption*/
    QString getAttrTdTh(const QString& attr);   /*td,th*/
    QString getAttrColcolGroup(const QString& attr);   /*col,colGroup*/
    QString getAttrTr(const QString& attr);   /*tr*/
    QString getAttrTbodyTHeadTFoot(const QString& attr);   /*tBody,tHead,tFoot*/
    QString getAttrTextArea(const QString& attr);   /*textArea*/

    QString getAttrTextRange(const QString& attr);   /*textRange*/
    QString getAttrTitle(const QString& attr);   /*title*/
    QString getAttrUl(const QString& attr);   /*ul*/
    QString getAttrHistory(const QString& attr);   /*history*/

private:
    CComQIPtr <IHTMLElement, &IID_IHTMLElement> spElement_;
    QString tagName_;
    QString id_;
    QString title_;
    QString innerText_;
    QString className_;
};

#endif
