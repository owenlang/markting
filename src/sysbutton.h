/*-----------------------------------------------------------------------------
    SysButton.h -- SysButton类的头文件
        主界面右上角的按钮
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#ifndef SYSBUTTON_H
#define SYSBUTTON_H

#include <QPushButton>

QT_BEGIN_NAMESPACE
class QPixmap;
QT_END_NAMESPACE

class SysButton : public QPushButton
{
    Q_OBJECT
public:
    explicit SysButton(QWidget *parent = 0);
    virtual ~SysButton();

protected:
    void paintEvent(QPaintEvent *);
    void enterEvent(QEvent *);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void leaveEvent(QEvent *);
signals:
    
public slots:

private:
    enum buttonStatus{NORMAL, ENTER, PRESS, NOSTATUS};
    buttonStatus status;

    QPixmap pixmap;

    int btnWidth;
    int btnHeight;

};

#endif // SYSBUTTON_H
