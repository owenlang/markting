﻿/*-----------------------------------------------------------------------------
    ProbeLabel.h -- ProbeLabel类的头文件
            (c) 灵魂战车.2013/10/27
 ----------------------------------------------------------------------------*/
#ifndef ProbeLabel_H
#define ProbeLabel_H

#include <QLabel>

class QMouseEvent;
class QPaintEvent;
class QWidget;

class ProbeLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ProbeLabel(QWidget *parent = 0);
    ~ProbeLabel();
    
signals:
    void clicked();
    void getArea(int);
    void endArea();

public slots:

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);

private:
    bool m_mousePress;
};

#endif // SANDBOX_LABEL_H
