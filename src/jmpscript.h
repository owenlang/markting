#ifndef JMPSCRIPT_H
#define JMPSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
QT_END_NAMESPACE

class JmpScript : public CScript
{
    Q_OBJECT
public:
    explicit JmpScript(QObject *parent = 0);
    
signals:
    
public slots:
    
};

#endif // JMPSCRIPT_H
