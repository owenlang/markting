#include "StdAfx.h"
#include "cscriptcontrol.h"

CScriptControl::CScriptControl(QObject *parent) :
    QObject(parent)
{
    initData();
}

void CScriptControl::initData()
{
    executeTimer_ = new QTimer(this);
    connect(executeTimer_, SIGNAL(timeout()), this, SLOT(executeTimerSlot()));

    oldIndex_ = 0;
    scriptIndex_ = 0;
}

void CScriptControl::runFinish(mark::RunDirect rd, int to)
{
    qDebug() << "run finish";
    //访问网页后延时
    if (immediateScriptList_[scriptIndex_]->getType() == CScript::XAccess) {
        DelayMs(500);
    }

    if (rd==mark::RUN_SUCCESS || rd==mark::RUN_JMP_TO) {
        immediateScriptList_[scriptIndex_]->stop();
        QObject::disconnect(immediateScriptList_[scriptIndex_], SIGNAL(runFinish(mark::RunDirect, int)), this, SLOT(runFinish(mark::RunDirect, int)));
        if (rd == mark::RUN_JMP_TO) {
            if (to >= immediateScriptList_.size()) {
                QMessageBox::warning(kPointerMainWidget, cn_tr("警告"), cn_tr("不存在跳转的脚本序号!"));
                scriptIndex_ = 0;
                return;
            }
            scriptIndex_ = to - 1;
        } else {
            ++ scriptIndex_;
        }

        qDebug() << "script index: " << scriptIndex_ << "list size: " << immediateScriptList_.size();
        if (scriptIndex_ < immediateScriptList_.size()) {
            QObject::connect(immediateScriptList_[scriptIndex_], SIGNAL(runFinish(mark::RunDirect, int)), this, SLOT(runFinish(mark::RunDirect, int)));
            CScript *script = immediateScriptList_[scriptIndex_];
            qDebug() << "run script number:" << scriptIndex_;
            script->run();
        } else {
            scriptIndex_ = 0;
        }
    }
}

void CScriptControl::run(const int &index)
{
    CScript *script = immediateScriptList_[index];
    script->run();
}

void CScriptControl::executeTimerSlot()
{    

}

void CScriptControl::runGroup()
{
    if (0 == immediateScriptList_.size()) {
        return;
    }
    QObject::connect(immediateScriptList_[0], SIGNAL(runFinish(mark::RunDirect, int)), this, SLOT(runFinish(mark::RunDirect, int)));

    immediateScriptList_[0]->run();
}

void CScriptControl::addScript(CScript *script)
{
    immediateScriptList_.push_back(script);
}

void CScriptControl::delScript(const int &index)
{
    delete immediateScriptList_[index];
    immediateScriptList_.removeAt(index);
}

void CScriptControl::clearAllScript()
{
    QList <CScript*>::iterator it;
    for (it = immediateScriptList_.begin(); it != immediateScriptList_.end(); ++it) {
        delete *it;
    }
    immediateScriptList_.clear();
}

void CScriptControl::swapScript(const int &index1, const int &index2)
{
    int listsize = immediateScriptList_.size();
    if (index1>listsize || index2>listsize || index1==index2) {
        return;
    }
    immediateScriptList_.swap(index1, index2);
}

void CScriptControl::printAllType()
{
    QList <CScript*>::iterator it;
    for (it = immediateScriptList_.begin(); it != immediateScriptList_.end(); ++it) {
        CScript *script = *it;
        script->printAllInfo();
    }
}

void CScriptControl::save(const QString &filename)
{
#if 0
    //保存为一组脚本(一个文件)
    //循环读取每条脚本的选项和值
    //放到一个 buffer 中(xml格式)
    //把每个 buffer 连接在一起,构成一组脚本
    //加密
    //保存为文件
#endif
}

void CScriptControl::load(const QString &filename)
{
#if 0
    //导入一组脚本(一个文件)
    //解密
    //循环读取每个命令节点
    //构造相应的脚本
    //设置脚本对应的选项和值
    //加入到命令列表
    //更新到显示
    //循环结束
#endif
}

CScriptControl::~CScriptControl()
{
    clearAllScript();
}
