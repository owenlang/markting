#include "StdAfx.h"
#include "ElementInfo.h"

ElementInfo::ElementInfo(IHTMLElement *pelement, QObject *parent) :
    QObject(parent), spElement_(pelement)
{
    initData();
}
void ElementInfo::initData()
{
    _bstr_t bstagname;
    if (SUCCEEDED(spElement_->get_tagName(bstagname.GetAddress()))) {
        tagName_ = QString::fromWCharArray(bstagname.GetBSTR());
    }
    _bstr_t bstagid;
    if (SUCCEEDED(spElement_->get_id(bstagid.GetAddress()))) {
        id_ = QString::fromWCharArray(bstagid.GetBSTR());
    }
    _bstr_t bstagtitle;
    if (SUCCEEDED(spElement_->get_title(bstagtitle.GetAddress()))) {
        title_ = QString::fromWCharArray(bstagtitle.GetBSTR());
    }
    _bstr_t bstaginnertext;
    if (SUCCEEDED(spElement_->get_innerText(bstaginnertext.GetAddress()))) {
        innerText_ = QString::fromWCharArray(bstaginnertext.GetBSTR());
    }
    _bstr_t bstagclass;
    if (SUCCEEDED(spElement_->get_className(bstagclass.GetAddress()))) {
        className_ = QString::fromWCharArray(bstagclass.GetBSTR());
    }
}

void ElementInfo::setAttrValue(const QString& attr, const QString& value)
{
    if (0 == attr.compare("border", Qt::CaseInsensitive)) { }
}

QString ElementInfo::getAttrValue(const QString& attr)
{
    if (0 == attr.compare("class"      , Qt::CaseInsensitive)){return className_;}
    if (0 == attr.compare("id"         , Qt::CaseInsensitive)){return id_;}
    if (0 == attr.compare("title"      , Qt::CaseInsensitive)){return title_;}
    if (0 == attr.compare(cn_tr("内容"), Qt::CaseInsensitive)){return innerText_;}
    if (0 == attr.compare("tagname"    , Qt::CaseInsensitive)){return tagName_;}

    if (0 == tagName_.compare("a"       , Qt::CaseInsensitive)){return getAttrAnchor(attr);}
    if (0 == tagName_.compare("area"    , Qt::CaseInsensitive)){return getAttrArea(attr);}
    if (0 == tagName_.compare("body"    , Qt::CaseInsensitive)){return getAttrBody(attr);}
    if (0 == tagName_.compare("br"      , Qt::CaseInsensitive)){return getAttrBr(attr);}
    if (0 == tagName_.compare("button"  , Qt::CaseInsensitive)){return getAttrButton(attr);}
    if (0 == tagName_.compare("dd"      , Qt::CaseInsensitive)){return getAttrDd(attr);}
    if (0 == tagName_.compare("window"  , Qt::CaseInsensitive)){return getAttrWindow(attr);}
    if (0 == tagName_.compare("div"     , Qt::CaseInsensitive)){return getAttrDiv(attr);}
    if (0 == tagName_.compare("dl"      , Qt::CaseInsensitive)){return getAttrDl(attr);}
    if (0 == tagName_.compare("document", Qt::CaseInsensitive)){return getAttrDocument(attr);}
    if (0 == tagName_.compare("dt"      , Qt::CaseInsensitive)){return getAttrDt(attr);}
    if (0 == tagName_.compare("font"    , Qt::CaseInsensitive)){return getAttrFont(attr);}
    if (0 == tagName_.compare("form"    , Qt::CaseInsensitive)){return getAttrForm(attr);}
    if (0 == tagName_.compare("frame"   , Qt::CaseInsensitive)){return getAttrFrame(attr);}
    if (0 == tagName_.compare("xml"     , Qt::CaseInsensitive)){return getAttrXml(attr);}
    if (0 == tagName_.compare("head"    , Qt::CaseInsensitive)){return getAttrHead(attr);}
    if (0 == tagName_.compare("h1-h6"   , Qt::CaseInsensitive)){return getAttrH1H6(attr);}
    if (0 == tagName_.compare("hr"      , Qt::CaseInsensitive)){return getAttrHr(attr);}
    if (0 == tagName_.compare("iframe"  , Qt::CaseInsensitive)){return getAttrIframe(attr);}
    if (0 == tagName_.compare("img"     , Qt::CaseInsensitive)){return getAttrImg(attr);}
    if (0 == tagName_.compare("input"     , Qt::CaseInsensitive)){return getAttrInput(attr);}
    if (0 == tagName_.compare("label"     , Qt::CaseInsensitive)){return getAttrLabel(attr);}
    if (0 == tagName_.compare("legend"     , Qt::CaseInsensitive)){return getAttrLegend(attr);}
    if (0 == tagName_.compare("li"     , Qt::CaseInsensitive)){return getAttrLi(attr);}
    if (0 == tagName_.compare("link"     , Qt::CaseInsensitive)){return getAttrLink(attr);}
    if (0 == tagName_.compare("menu"     , Qt::CaseInsensitive)){return getAttrMenu(attr);}
    if (0 == tagName_.compare("location"     , Qt::CaseInsensitive)){return getAttrLocation(attr);}
    if (0 == tagName_.compare("map"     , Qt::CaseInsensitive)){return getAttrMap(attr);}
    if (0 == tagName_.compare("marquee"     , Qt::CaseInsensitive)){return getAttrMarquee(attr);}
    if (0 == tagName_.compare("meta"     , Qt::CaseInsensitive)){return getAttrMeta(attr);}
    if (0 == tagName_.compare("object"     , Qt::CaseInsensitive)){return getAttrObject(attr);}
    if (0 == tagName_.compare("ol"     , Qt::CaseInsensitive)){return getAttrOl(attr);}
    if (0 == tagName_.compare("userProfile"     , Qt::CaseInsensitive)){return getAttrUserProfile(attr);}
    if (0 == tagName_.compare("option"     , Qt::CaseInsensitive)){return getAttrOption(attr);}
    if (0 == tagName_.compare("p"     , Qt::CaseInsensitive)){return getAttrP(attr);}
    if (0 == tagName_.compare("ins"     , Qt::CaseInsensitive)){return getAttrIns(attr);}
    if (0 == tagName_.compare("del"     , Qt::CaseInsensitive)){return getAttrDel(attr);}
    if (0 == tagName_.compare("embed"     , Qt::CaseInsensitive)){return getAttrEmbed(attr);}
    if (0 == tagName_.compare("popup"     , Qt::CaseInsensitive)){return getAttrPopup(attr);}
    if (0 == tagName_.compare("screen"     , Qt::CaseInsensitive)){return getAttrScreen(attr);}
    if (0 == tagName_.compare("script"     , Qt::CaseInsensitive)){return getAttrScript(attr);}
    if (0 == tagName_.compare("select"     , Qt::CaseInsensitive)){return getAttrSelect(attr);}
    if (0 == tagName_.compare("span"     , Qt::CaseInsensitive)){return getAttrSpan(attr);}
    if (0 == tagName_.compare("page"     , Qt::CaseInsensitive)){return getAttrPage(attr);}
    if (0 == tagName_.compare("table"     , Qt::CaseInsensitive)){return getAttrTable(attr);}
    if (0 == tagName_.compare("caption"     , Qt::CaseInsensitive)){return getAttrCaption(attr);}
    if (0 == tagName_.compare("td"     , Qt::CaseInsensitive) ||
        0 == tagName_.compare("th"     , Qt::CaseInsensitive)){return getAttrTdTh(attr);}
    if (0 == tagName_.compare("col" , Qt::CaseInsensitive) ||
        0 == tagName_.compare("colGroup" , Qt::CaseInsensitive)) {return getAttrColcolGroup(attr);}
    if (0 == tagName_.compare("tr"     , Qt::CaseInsensitive)){return getAttrTr(attr);}
    if (0 == tagName_.compare("tbody"     , Qt::CaseInsensitive) ||
        0 == tagName_.compare("thead"     , Qt::CaseInsensitive) ||
        0 == tagName_.compare("tfoot"     , Qt::CaseInsensitive) ){return getAttrTbodyTHeadTFoot(attr);}
    if (0 == tagName_.compare("textarea"     , Qt::CaseInsensitive)){return getAttrTextArea(attr);}
    if (0 == tagName_.compare("teatrange"     , Qt::CaseInsensitive)){return getAttrTextRange(attr);}
    if (0 == tagName_.compare("title"     , Qt::CaseInsensitive)){return getAttrTitle(attr);}
    if (0 == tagName_.compare("ul"     , Qt::CaseInsensitive)){return getAttrUl(attr);}
    if (0 == tagName_.compare("history"     , Qt::CaseInsensitive)){return getAttrHistory(attr);}

    return QString();
}

QString ElementInfo::getAttrElement(const QString& attr){   return QString();}   /*element*/
QString ElementInfo::getAttrAnchor(const QString& attr)
{
    CComQIPtr <IHTMLAnchorElement, &IID_IHTMLAnchorElement> spAnchor;
    spAnchor = spElement_;
    _bstr_t bsval;
    if (0 == attr.compare("href", Qt::CaseInsensitive)) {
        spAnchor->get_href(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    if (0 == attr.compare("name", Qt::CaseInsensitive)) {
        spAnchor->get_name(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    return QString();
}   /*a*/
QString ElementInfo::getAttrArea(const QString& attr)
{return QString();}   /*area*/
QString ElementInfo::getAttrBody(const QString& attr)
{return QString();}   /*body*/
QString ElementInfo::getAttrBr(const QString& attr)
{return QString();}   /*br*/
QString ElementInfo::getAttrButton(const QString& attr)
{return QString();}   /*button*/
QString ElementInfo::getAttrDd(const QString& attr)
{return QString();}   /*dd*/
QString ElementInfo::getAttrWindow(const QString& attr)
{return QString();}   /*window*/
QString ElementInfo::getAttrDiv(const QString& attr)
{return QString();}   /*div*/
QString ElementInfo::getAttrDl(const QString& attr)
{return QString();}   /*dl*/
QString ElementInfo::getAttrDocument(const QString& attr)
{return QString();}   /*document*/
QString ElementInfo::getAttrDt(const QString& attr){return QString();}   /*dt*/
QString ElementInfo::getAttrFont(const QString& attr){return QString();}   /*font*/
QString ElementInfo::getAttrForm(const QString& attr){return QString();}   /*form*/
QString ElementInfo::getAttrFrame(const QString& attr){return QString();}   /*frame*/
QString ElementInfo::getAttrXml(const QString& attr){return QString();}   /*xml*/
QString ElementInfo::getAttrHead(const QString& attr){return QString();}   /*head*/
QString ElementInfo::getAttrH1H6(const QString& attr){return QString();}   /*h1 through h6*/
QString ElementInfo::getAttrHr(const QString& attr){return QString();}   /*hr*/
QString ElementInfo::getAttrHtml(const QString& attr){return QString();}   /*html element*/
QString ElementInfo::getAttrIframe(const QString& attr){return QString();}   /*iframe*/
QString ElementInfo::getAttrImg(const QString& attr)
{
    CComQIPtr <IHTMLImgElement, &IID_IHTMLImgElement> spImg;
    spImg = spElement_;
    _bstr_t bsval;
    if (0 == attr.compare("src", Qt::CaseInsensitive)) {
        spImg->get_src(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    if (0 == attr.compare("href", Qt::CaseInsensitive)) {
        spImg->get_href(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    return QString();
}   /*img*/
QString ElementInfo::getAttrInput(const QString& attr)
{
    CComQIPtr <IHTMLInputElement, &IID_IHTMLInputElement> spInput;
    spInput = spElement_;
    _bstr_t bsval;
    if (0 == attr.compare("type", Qt::CaseInsensitive)) {
        spInput->get_type(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    if (0 == attr.compare("value", Qt::CaseInsensitive)) {
        spInput->get_value(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    if (0 == attr.compare("name", Qt::CaseInsensitive)) {
        spInput->get_name(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    if (0 == attr.compare("src", Qt::CaseInsensitive)) {
        spInput->get_src(bsval.GetAddress());
        return QString::fromWCharArray(bsval.GetBSTR());
    }
    return QString();
}   /*input*/
QString ElementInfo::getAttrLabel(const QString& attr){return QString();}   /*label*/
QString ElementInfo::getAttrLegend(const QString& attr){return QString();}   /*legend*/

QString ElementInfo::getAttrLi(const QString& attr){return QString();}   /*li*/
QString ElementInfo::getAttrLink(const QString& attr){return QString();}   /*link*/
QString ElementInfo::getAttrMenu(const QString& attr){return QString();}   /*menu*/
QString ElementInfo::getAttrLocation(const QString& attr){return QString();}   /*location*/
QString ElementInfo::getAttrMap(const QString& attr){return QString();}   /*map*/
QString ElementInfo::getAttrMarquee(const QString& attr){return QString();}   /*marquee*/
QString ElementInfo::getAttrMeta(const QString& attr){return QString();}   /*meta*/
QString ElementInfo::getAttrObject(const QString& attr){return QString();}   /*object*/
QString ElementInfo::getAttrOl(const QString& attr){return QString();}   /*ol*/
QString ElementInfo::getAttrUserProfile(const QString& attr){return QString();}   /*userProfile*/
QString ElementInfo::getAttrOption(const QString& attr){return QString();}   /*option*/
QString ElementInfo::getAttrP(const QString& attr){return QString();}   /*p*/

QString ElementInfo::getAttrIns(const QString& attr){return QString();}   /*ins*/
QString ElementInfo::getAttrDel(const QString& attr){return QString();}   /*del*/
QString ElementInfo::getAttrEmbed(const QString& attr){return QString();}   /*embed*/
QString ElementInfo::getAttrPopup(const QString& attr){return QString();}   /*popup*/
QString ElementInfo::getAttrScreen(const QString& attr){return QString();}   /*screen*/
QString ElementInfo::getAttrScript(const QString& attr){return QString();}   /*script*/
QString ElementInfo::getAttrSelect(const QString& attr){return QString();}   /*select*/
QString ElementInfo::getAttrSpan(const QString& attr){return QString();}   /*span*/

QString ElementInfo::getAttrPage(const QString& attr){return QString();}   /*page*/
QString ElementInfo::getAttrTable(const QString& attr){return QString();}   /*table*/
QString ElementInfo::getAttrCaption(const QString& attr){return QString();}   /*caption*/
QString ElementInfo::getAttrTdTh(const QString& attr){return QString();}   /*td,th*/
QString ElementInfo::getAttrColcolGroup(const QString& attr){return QString();}   /*col,colGroup*/
QString ElementInfo::getAttrTr(const QString& attr){return QString();}   /*tr*/
QString ElementInfo::getAttrTbodyTHeadTFoot(const QString& attr){return QString();}   /*tBody,tHead,tFoot*/
QString ElementInfo::getAttrTextArea(const QString& attr){return QString();}   /*textArea*/

QString ElementInfo::getAttrTextRange(const QString& attr){return QString();}   /*textRange*/
QString ElementInfo::getAttrTitle(const QString& attr){return QString();}   /*title*/
QString ElementInfo::getAttrUl(const QString& attr){return QString();}   /*ul*/
QString ElementInfo::getAttrHistory(const QString& attr){return QString();}   /*history*/



ElementInfo::~ElementInfo()
{

}
