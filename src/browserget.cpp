#include "StdAfx.h"
#include "BrowserGet.h"
#include "ui_mainwidget.h"

BrowserGet* BrowserGet::self = NULL;
BrowserGet* BrowserGet::getInstance()
{
    if (NULL == self) {
        self = new BrowserGet();
    }
    return self;
}

BrowserGet::BrowserGet(QObject *parent) :
    QObject(parent)
{
    m_pEvt = new BrowserEvent();

    QObject::connect(m_pEvt, SIGNAL(loadComplete()), this, SIGNAL(loadComplete()));

}

//从窗口句柄获取其类名
QString BrowserGet::getClassNameFromHandle(HWND hwnd)
{
    if (NULL == hwnd) {
        qDebug("hwnd is null");
        return NULL;
    }
    char strClass[200]="\0";
    ::GetClassNameA(hwnd,strClass,200);
    QString strClassName = cn_tr(strClass);
    return strClassName;
}


HRESULT BrowserGet::getIWebBrowser2(HWND hwnd)
{
    if (NULL == hwnd) {
        return S_FALSE;
    }
    QString strClassName = getClassNameFromHandle(hwnd);
    if (0 != QString::compare(strClassName, cn_tr("Internet Explorer_Server"))) {
        return S_FALSE;
    }

    HINSTANCE hInst = ::LoadLibraryA("OLEACC.DLL");
    LRESULT lRes;
    UINT nMsg = ::RegisterWindowMessageA("WM_HTML_GETOBJECT");
    ::SendMessageTimeout(hwnd, nMsg, 0L, 0L, SMTO_ABORTIFHUNG, 1000, (DWORD*)&lRes);
    LPFNOBJECTFROMLRESULT  pfObjectFromLresult = (LPFNOBJECTFROMLRESULT)::GetProcAddress(hInst, "ObjectFromLresult");
    if (pfObjectFromLresult != NULL) {
        HRESULT hr;
        //获取WebBrowser2
        CComPtr<IServiceProvider> spServiceProv;
        hr = (*pfObjectFromLresult)(lRes, IID_IServiceProvider, 0, (void**)&spServiceProv);
        if (SUCCEEDED(hr)) {
            CComPtr <IWebBrowser2> spWebBrowser2;
            hr = spServiceProv->QueryService(SID_SWebBrowserApp, IID_IWebBrowser2,(void**)&spWebBrowser2);
            if (SUCCEEDED(hr)) {
                spWebBrowser2_ = spWebBrowser2;
                qDebug("Get the IID_IWebBrowser2 successful!");
            }
        }
    } else {
        QMessageBox::information(NULL, cn_tr("提示"), cn_tr("请您安装Microsoft Active Accessibility"));
    }
    ::FreeLibrary(hInst);
    return S_OK;
}

void BrowserGet::installWebEventsHandle()
{
    if (NULL == spWebBrowser2_) {
        return;
    }
    //捕获浏览器事件
    HRESULT hr;
    m_pEvt->m_pBrowser = spWebBrowser2_;
    //如果还存在连接，则断开连接
    uninstallWebEventsHandle();
    m_pEvt->uninstallDocEventsHandle();

    //2.找到连接点容器
    CComPtr <IConnectionPointContainer> pContainer;
    hr = spWebBrowser2_->QueryInterface(IID_IConnectionPointContainer, (void**)&pContainer);
    if (SUCCEEDED(hr)) {
        qDebug("Query IID_IConnectionPointContainer success.");
        //3.从连接点容器中找到DIID_DWebBrowserEvents2 连接点
        hr = pContainer->FindConnectionPoint(DIID_DWebBrowserEvents2, &(m_pEvt->m_pPoint));
        if (SUCCEEDED(hr)) {
            qDebug("Find DIID_DWebBrowserEvents2 connect point success.");
            //连接到DIID_DWebBrowserEvents2
            CComPtr <IUnknown> pUnk;
            m_pEvt->QueryInterface(IID_IUnknown, (void**)&pUnk);
            m_pEvt->m_pPoint->Advise(pUnk, &(m_pEvt->m_dwCookie));
            if (SUCCEEDED(hr)) {
                qDebug("DIID_DWebBrowserEvents2 Advise success.");
                return m_pEvt->installDocEventsHandle();
            }
        }
        qDebug("Find DIID_DWebBrowserEvents2 connect point failed.");
    }
    qDebug("Query IID_IConnectionPointContainer failed.");
}


void BrowserGet::uninstallWebEventsHandle()
{
    //如果还存在连接，则断开连接
    if (m_pEvt->m_dwCookie || m_pEvt->m_pPoint) {
        qDebug("Exist DIID_DWebBrowserEvents2 connect point, disconnect it.");
        m_pEvt->m_pPoint->Unadvise(m_pEvt->m_dwCookie);
        m_pEvt->m_dwCookie = 0;
        m_pEvt->m_pPoint = NULL;
        return m_pEvt->uninstallDocEventsHandle();
    }
}

void BrowserGet::setJudgeUrl(const QString& url, const QString& condition)
{
    m_pEvt->useJudgeComplete(url, condition);
}

void BrowserGet::cancelJudgeUrl()
{
    m_pEvt->cancelJudgeComplete();    
}

void BrowserGet::openUrl(const QString &url)
{
    _variant_t vUrl(url.toLocal8Bit().data());
    spWebBrowser2_->Navigate2(&vUrl, NULL, NULL, NULL, NULL); //_variant_t使用 GetAddress失效
}

QString BrowserGet::getHtml()
{
    QString html;
    qDebug() << html;
    if (!spWebBrowser2_) {
        qDebug() << "spWebBrowser2_ is null";
        return html;
    }
    HRESULT hr;
    CComQIPtr <IHTMLDocument2, &IID_IHTMLDocument2> spdoc2;
    CComPtr <IDispatch> spdisp;
    hr = spWebBrowser2_->get_Document(&spdisp);
    spdoc2 = spdisp;
    if (SUCCEEDED(hr) && spdoc2) {
        CComQIPtr <IHTMLElement, &IID_IHTMLElement> spbody;
        hr = spdoc2->get_body(&spbody);
        if (SUCCEEDED(hr) && spbody) {
            _bstr_t bshtml;
            hr = spbody->get_outerHTML(bshtml.GetAddress());
            html = QString::fromWCharArray(bshtml.GetBSTR());
            if (SUCCEEDED(hr)) {
                qDebug() << "get html text success.";
            }
        }
    }
    return html;
}

BrowserGet::~BrowserGet()
{
    delete m_pEvt;
}
