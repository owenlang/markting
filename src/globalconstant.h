#ifndef GLOBALCONSTANT_H
#define GLOBALCONSTANT_H

namespace mark {
typedef enum {
    RUN_SUCCESS = 0,//运行成功,继续运行
    RUN_FAILED, //运行失败
    RUN_JMP_TO  //运行功能,跳转到
}RunDirect;

typedef enum {
    NO_PROBE = 0,
    USE_PROBE
}ProbeState;

}

extern mark::ProbeState kProbeState;



#endif // GLOBALCONSTANT_H
