#ifndef BrowserGet_H
#define BrowserGet_H

#include <QObject>
class BrowserEvent;

class BrowserGet : public QObject
{
    Q_OBJECT
public:
    explicit BrowserGet(QObject *parent = 0);
    static BrowserGet* getInstance();
    ~BrowserGet();
    //根据窗口句柄取得窗口类名
    QString getClassNameFromHandle(HWND hwnd);
    //根据窗口句柄取得IWebBrowser2 接口指针
    HRESULT getIWebBrowser2(HWND hwnd);
    //安装事件连接点
    void installWebEventsHandle();
    //卸载事件连接点
    void uninstallWebEventsHandle();
    void setJudgeUrl(const QString& url, const QString& condition);
    void cancelJudgeUrl();

    void useProbe();
    void openUrl(const QString& url);
    QString getHtml();

signals:
    void loadComplete();

public slots:

public:
    CComPtr <IWebBrowser2> spWebBrowser2_;
    BrowserEvent *m_pEvt;
    static BrowserGet *self;
};

#endif // BrowserGet_H
