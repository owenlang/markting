/*-----------------------------------------------------------------------------
Description: 接收处理 DIID_DWebBrowserEvents2 事件的类
Author: WRW.owenlang@163.com
Date: 2013/11/06
-----------------------------------------------------------------------------*/
#ifndef BrowserEvent_H
#define BrowserEvent_H

#include <oaidl.h>    //包含了IDspatch接口定义
#include <Mshtml.h>   //事件IID的定义
#include <ExDisp.h>   //some IID defined
#include <ExDispid.h> //some DISPID defined
#include <MsHtmdid.h> //more DISPID defined
#include <QObject>

class HTMLDocEvent;
class QString;


class BrowserEvent : public QObject, public IDispatch
{
    Q_OBJECT
public:
    BrowserEvent(QObject *parent=0);
    virtual ~BrowserEvent();

    HRESULT STDMETHODCALLTYPE QueryInterface(const struct _GUID &iid, void ** ppv)
    {
        if (iid == IID_IUnknown) {
            *ppv = static_cast<IUnknown*>(this);
        } else if (iid == IID_IDispatch) {
            *ppv = static_cast<IDispatch*>(this);
        } else {
            return E_NOINTERFACE;
        }
        return S_OK;
    }

    ULONG STDMETHODCALLTYPE AddRef(void)
    { return 1; }// 做个假的就可以，因为反正这个对象在程序结束前是不会退出的

    ULONG STDMETHODCALLTYPE Release(void)
    { return 0; } // 做个假的就可以，因为反正这个对象在程序结束前是不会退出的

    HRESULT  STDMETHODCALLTYPE GetTypeInfoCount(
                /* [out] */ UINT *pctinfo) ;

    HRESULT  STDMETHODCALLTYPE GetTypeInfo(
        /* [in] */ UINT iTInfo,
        /* [in] */ LCID lcid,
        /* [out] */ ITypeInfo **ppTInfo);

    HRESULT  STDMETHODCALLTYPE GetIDsOfNames(
        /* [in] */ REFIID riid,
        /* [size_is][in] */ LPOLESTR *rgszNames,
        /* [in] */ UINT cNames,
        /* [in] */ LCID lcid,
        /* [size_is][out] */ DISPID *rgDispId) ;

    HRESULT  STDMETHODCALLTYPE Invoke(
        /* [in] */ DISPID dispIdMember,
        /* [in] */ REFIID riid,
        /* [in] */ LCID lcid,
        /* [in] */ WORD wFlags,
        /* [out][in] */ DISPPARAMS *pDispParams,
        /* [out] */ VARIANT *pVarResult,
        /* [out] */ EXCEPINFO *pExcepInfo,
        /* [out] */ UINT *puArgErr) ;
        
signals:
    void loadComplete();

public slots:


public:
	//安装 DIID_HTMLDocumentEvents2 事件接收器
    void installDocEventsHandle();
    //卸载 DIID_HTMLDocumentEvents2 事件接收器
    void uninstallDocEventsHandle();
    void useJudgeComplete(const QString& url, const QString& condition);
    void cancelJudgeComplete();

public:
    CComPtr <IConnectionPoint> m_pPoint; //连接点
    DWORD m_dwCookie;
    CComPtr <IHTMLDocument2> m_pDoc;

    CComPtr <IWebBrowser2> m_pBrowser;
    HTMLDocEvent *m_pDocEvt;

private:
    bool useJudge_; //是否进行判断
    QString url_;
    QString condition_; //判断条件,==,!=,包含
};

#endif // BrowserEvent_H
