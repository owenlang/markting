#include "StdAfx.h"
#include "remoteverify.h"
#include <stdio.h>

RemoteVerify::RemoteVerify(QWidget *parent) :
    QWidget(parent), tempDirName_("./temp")
{
    setupUi(this);
    initData();
}

void RemoteVerify::initData()
{
    loginState_ = false;
    uuwise = new QLibrary("UUWiseHelper");
    uuwise->load();
    if (!uuwise->isLoaded()) {
        qDebug("Load UUWiseHelper failed");
    }
    this->hide();

    uu_setSoftInfoA = (UUWiseSoftInfoPrototype)uuwise->resolve("uu_setSoftInfoA");
    if (!uu_setSoftInfoA) { qDebug("get uu_setSoftInfoA address failed");  return;}
    uu_loginA = (UUWiseLoginPrototype)uuwise->resolve("uu_loginA");
    if (!uu_loginA) { qDebug("get uu_loginA address failed");  return;}
    uu_recognizeByCodeTypeAndPathA = (UUWiseRecognize)uuwise->resolve("uu_recognizeByCodeTypeAndPathA");
    if (!uu_recognizeByCodeTypeAndPathA) { qDebug("get uu_recognizeByCodeTypeAndPathA failed."); return; }

}

void RemoteVerify::on_okBtn_clicked()
{
    this->hide();
    login();
}

void RemoteVerify::setPicture(const QString& filename)
{
    QString code;
    if (!loginState_) {
        if (!login()) {
            emit getCode(code);
        }
    }
    char recoResult[50];
    long codeID = 0;
    qDebug() << cn_tr("开始调用识别函数,请耐心等待返回结果……\n");
    QString path = tempDirName_ + "/" + filename;
    codeID = uu_recognizeByCodeTypeAndPathA(path.toLocal8Bit().data(), 1, recoResult); //第四步调用识别函数
    if (codeID > 0) {
        qDebug() << cn_tr("识别完成,验证码ID为:") << codeID << cn_tr(",识别结果为:") << cn_tr(recoResult);
        code = recoResult;
    } else {
        qDebug() << cn_tr("error code:") << codeID;
    }

    emit getCode(code);
}
bool RemoteVerify::login()
{
    char *user_name = "pengshaoxie";//userNameEdit->text().trimmed().toLocal8Bit().data();
    char *user_password = "qxieshaopeng";//userPasswordEdit->text().trimmed().toLocal8Bit().data();
    int softID = 94071;
    char *softKey="d7ba37ce823645caa8822feda1cf6e95";

    long login_status = 0,  score = 0;

    uu_setSoftInfoA(softID, softKey);  //第一步,接入UUWise

    login_status = uu_loginA(user_name, user_password); //第二步,登录
    if (login_status) {
        UUWiseLoginPrototype uu_getScoreA = (UUWiseLoginPrototype)uuwise->resolve("uu_getScoreA");
        if (!uu_getScoreA) { qDebug("get uu_getScoreA failed."); return false; }
        score = uu_getScoreA(user_name, user_password); //第三步,获取题分
        qDebug() << cn_tr("恭喜您，登录成功，您的用户ID为：") << login_status << cn_tr("剩余") << score;
        loginState_ = true;
        return true;
    } else {
         qDebug() << cn_tr("login fail, error code:") << login_status;
        return false;
    }
}


void RemoteVerify::getFileMd5(LPCWSTR FileDirectory)
{
    FileDirectory = FileDirectory;
}

RemoteVerify::~RemoteVerify()
{
    if (uuwise->isLoaded()) {
        uuwise->unload();
    }
    delete uuwise;
}
