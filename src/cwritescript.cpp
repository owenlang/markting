#include "StdAfx.h"
#include "cwritescript.h"
#include "ui_MainWidget.h"

CWriteScript::CWriteScript(QObject *parent) :
    QObject(parent)
{
    initData();
}

void CWriteScript::initData()
{
    //初始化脚本列表数据模型
    immediateModel_ = new QStandardItemModel();
    immediateModel_->setHorizontalHeaderItem(0, new QStandardItem(cn_tr("动作")));
    immediateModel_->setHorizontalHeaderItem(1, new QStandardItem(cn_tr("内容")));
    //初始化探测窗口定时器
    getAreatimer_ = new QTimer(this);
    QObject::connect(getAreatimer_, SIGNAL(timeout()), this, SLOT(onGetArea()));
    //初始化获取浏览器接口对象
    browserGet_ = BrowserGet::getInstance();
    //脚本控制类
    scriptControler_ = new CScriptControl();
    remoteVerify_ = new RemoteVerify();
    findElement_ = new FindElement();
}

void CWriteScript::endGetAreaTimer()
{
    getAreatimer_->stop();
    HRESULT hr = browserGet_->getIWebBrowser2(hwnd_);
    if (FAILED(hr)) {
        return;
    }
    browserGet_->installWebEventsHandle();
}

void CWriteScript::getArea(const int& time)
{
    getAreatimer_->start(time);
}

void CWriteScript::probeSlot()
{
    //如果是在探测状态下再次点击,则取消连接
    if (0 == QString::compare(cn_tr("取消"), kPointerMainWidget->ui->probeBtn->text())) {
        kPointerMainWidget->ui->probeBtn->setText(cn_tr("探测"));
        kProbeState = mark::NO_PROBE;
    } else {
        browserGet_->installWebEventsHandle();
        kPointerMainWidget->ui->probeBtn->setText(cn_tr("取消"));
        kProbeState = mark::USE_PROBE;
    }
}

void CWriteScript::findElementSlot()
{
    findElement_->show();
}

void CWriteScript::remoteVerifySlot()
{
    remoteVerify_->show();
}

//添加脚本--访问网页
void CWriteScript::addAccScriptSlot()
{
    //构造一条脚本
    AccessWebScript *script = new AccessWebScript();
    //给脚本添加选项,只添加选定的项
    QString str;
    //不判断,因为网址可空
    script->add(script->getopt(AccessWebScript::AccUrl), kPointerMainWidget->ui->urlEdit->text());
    str += kPointerMainWidget->ui->urlEdit->text();    

    if (Qt::Checked == kPointerMainWidget->ui->accClearAllTextCheckBox->checkState()) {
        script->add(script->getopt(AccessWebScript::AccClearAllText), QString("1"));
        str += "+" + kPointerMainWidget->ui->accClearAllTextCheckBox->text();
    }

    if (Qt::Checked == kPointerMainWidget->ui->accClearCookiesCheckBox->checkState()) {
        script->add(script->getopt(AccessWebScript::AccClearCookies), QString("1"));
        str += "+" + kPointerMainWidget->ui->accClearCookiesCheckBox->text();
    }

    if (Qt::Checked == kPointerMainWidget->ui->accDisablePopupCheckBox->checkState()) {
        script->add(script->getopt(AccessWebScript::AccDisablePopup), QString("1"));
        str += "+" + kPointerMainWidget->ui->accDisablePopupCheckBox->text();
    }

    if (Qt::Checked == kPointerMainWidget->ui->accUseProxyCheckBox->checkState()) {
        script->add(script->getopt(AccessWebScript::AccUseProxy), QString("1"));
        str += "+" + kPointerMainWidget->ui->accUseProxyCheckBox->text();
    }

    if (Qt::Checked == kPointerMainWidget->ui->accRefreshCheckBox->checkState()) {
        script->add(script->getopt(AccessWebScript::AccRefresh), QString("1"));
        str += "+" + kPointerMainWidget->ui->accRefreshCheckBox->text();
    }
    //更新显示到即时列表
    QList<QStandardItem*> item;
    item.push_back(new QStandardItem(cn_tr("访问网页")));
    item.push_back(new QStandardItem(str));
    immediateModel_->appendRow(item);
    //把脚本放入一个控制类中的list
    scriptControler_->addScript(script);
}
//添加脚本--网页填表
void CWriteScript::addFillScriptSlot()
{    
    if (       kPointerMainWidget->ui->tagTagNameEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagTypeEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagClassEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagIdEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagTitleEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagNameEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagValueEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagSrcEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagHrefEdit->text().trimmed().isEmpty()
            && kPointerMainWidget->ui->tagContentEdit->text().isEmpty()) {
        QMessageBox::warning(NULL, cn_tr("提示"), cn_tr("没有探测信息!"), QMessageBox::Ok);
        return;
    }
    if (!kPointerMainWidget->ui->clickRadioBtn->isChecked() && !kPointerMainWidget->ui->fillSetAttrRadioBtn->isChecked() &&
            !kPointerMainWidget->ui->setRadioBtn->isChecked() && !kPointerMainWidget->ui->fillVerifyCodeRadioBtn->isChecked()) {
            QMessageBox::warning(kPointerMainWidget, cn_tr("提示"), cn_tr("没有选择任何操作!"), QMessageBox::Ok);
            return;
    }
    QString str; //用于显示在脚本列表
    FillWebScript *script = new FillWebScript();
    if (kPointerMainWidget->ui->clickRadioBtn->isChecked()) {
        script->add(script->getopt(FillWebScript::FillClickRadio), QString("1"));
        str += kPointerMainWidget->ui->clickRadioBtn->text() + kPointerMainWidget->ui->tagIdEdit->text();
    }
    if (kPointerMainWidget->ui->fillSetAttrRadioBtn->isChecked()) {
        if (kPointerMainWidget->ui->fillAttrNameEdit->text().trimmed().isEmpty()) { //trimmed 去掉字符串后面的空格
            QMessageBox::warning(kPointerMainWidget, cn_tr("提示"), cn_tr("属性名为空"), QMessageBox::Ok);
            return;
        }
        script->add(script->getopt(FillWebScript::FillSetAttrRadio), QString("1"));
        str += kPointerMainWidget->ui->fillSetAttrRadioBtn->text();
        script->add(script->getopt(FillWebScript::FillAttrNameEdit), kPointerMainWidget->ui->fillAttrNameEdit->text().trimmed());//加入属性名
        script->add(script->getopt(FillWebScript::FillAttrValueEdit), kPointerMainWidget->ui->fillAttrValueEdit->text().trimmed());//加入属性值
        str += "+" + kPointerMainWidget->ui->fillAttrNameEdit->text().trimmed() +
                "=" + kPointerMainWidget->ui->fillAttrValueEdit->text().trimmed();
    }
    if (kPointerMainWidget->ui->setRadioBtn->isChecked()) {
        script->add(script->getopt(FillWebScript::FillSetTextRadio), QString("1"));
        str += kPointerMainWidget->ui->setRadioBtn->text();
        script->add(script->getopt(FillWebScript::FillSetTextEdit), kPointerMainWidget->ui->fillSetTextEdit->toPlainText().trimmed());
        str += "+" + kPointerMainWidget->ui->fillSetTextEdit->toPlainText().trimmed();
    }
    if (kPointerMainWidget->ui->fillVerifyCodeRadioBtn->isChecked()) {
        script->add(script->getopt(FillWebScript::FillGetVerifyCodeRadio), QString("1"));
        str += kPointerMainWidget->ui->fillVerifyCodeRadioBtn->text();
        if (kPointerMainWidget->ui->verifyManRadioBtn->isChecked()) {
            script->add(script->getopt(FillWebScript::FillMannualRadioBtn), QString("1"));
            str += ("+" + kPointerMainWidget->ui->verifyManRadioBtn->text());
        } else if (kPointerMainWidget->ui->verifyRemoteRadioBtn->isChecked()) {
            script->add(script->getopt(FillWebScript::FillRemoteRadioBtn), QString("1"));
            str += ("+" + kPointerMainWidget->ui->verifyRemoteRadioBtn->text());
        } else {
            QMessageBox::warning(kPointerMainWidget, cn_tr("提示"), cn_tr("没有选择验证码获取方式!"), QMessageBox::Ok);
            return;
        }
    }
    //保存探测信息
    script->add(script->getopt(FillWebScript::FillTagTagName), kPointerMainWidget->ui->tagTagNameEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagType), kPointerMainWidget->ui->tagTypeEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagClass), kPointerMainWidget->ui->tagClassEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagId), kPointerMainWidget->ui->tagIdEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagTitle), kPointerMainWidget->ui->tagTitleEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagName), kPointerMainWidget->ui->tagNameEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagValue), kPointerMainWidget->ui->tagValueEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagSrc), kPointerMainWidget->ui->tagSrcEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagHref), kPointerMainWidget->ui->tagHrefEdit->text().trimmed());
    script->add(script->getopt(FillWebScript::FillTagContent), kPointerMainWidget->ui->tagContentEdit->text().trimmed());

    //将脚本添加到显示模型
    QList<QStandardItem*> item;
    item.push_back(new QStandardItem(cn_tr("网页填表")));
    item.push_back(new QStandardItem(str));
    immediateModel_->appendRow(item);
    //放到脚本列表中
    scriptControler_->addScript(script);
}

void CWriteScript::addConditionScriptSlot ()
{
    QString str = cn_tr("如果");
    ConditionScript *script = new ConditionScript();
    if (kPointerMainWidget->ui->conditionAttrNameEdit->text().trimmed().isEmpty()
            || kPointerMainWidget->ui->conditionAttrValueEdit->text().trimmed().isEmpty()) {
        QMessageBox::warning(kPointerMainWidget, cn_tr("提示"), cn_tr("没有填写!"), QMessageBox::Ok);
        return;
    }
    if (!kPointerMainWidget->ui->continueRadioBtn->isChecked()
            && !kPointerMainWidget->ui->jmpToRadioBtn->isChecked()
            && !kPointerMainWidget->ui->waitRadioBtn->isChecked()) {
        QMessageBox::warning(kPointerMainWidget, cn_tr("提示"), cn_tr("没有选择!"), QMessageBox::Ok);
        return;
    }
    if (!kPointerMainWidget->ui->conditionUrlValueEdit->text().trimmed().isEmpty()) {
        script->add(script->getopt(ConditionScript::ConditionUrlSymbolComBox), kPointerMainWidget->ui->conditionUrlSymbolComBox->currentText());
        str += (cn_tr("网址") + kPointerMainWidget->ui->conditionUrlSymbolComBox->currentText());
        script->add(script->getopt(ConditionScript::ConditionUrlValueEdit), kPointerMainWidget->ui->conditionUrlValueEdit->text().trimmed());
        str += kPointerMainWidget->ui->conditionUrlValueEdit->text().trimmed();
    }

    script->add(script->getopt(ConditionScript::ConditionAttrNameEdit), kPointerMainWidget->ui->conditionAttrNameEdit->text().trimmed());
    str += kPointerMainWidget->ui->conditionAttrNameEdit->text().trimmed();
    script->add(script->getopt(ConditionScript::ConditionAttrSymbolComBox), kPointerMainWidget->ui->conditionAttrSymbolComBox->currentText());
    str += kPointerMainWidget->ui->conditionAttrSymbolComBox->currentText();
    script->add(script->getopt(ConditionScript::ConditionAttrValueEdit), kPointerMainWidget->ui->conditionAttrValueEdit->text().trimmed());
    str += kPointerMainWidget->ui->conditionAttrValueEdit->text().trimmed();

    if (!kPointerMainWidget->ui->conditionTagNameEdit->text().trimmed().isEmpty()) {
        script->add(script->getopt(ConditionScript::ConditionTagNameEdit), kPointerMainWidget->ui->conditionTagNameEdit->text().trimmed());
        str += (cn_tr("标签名为") + kPointerMainWidget->ui->conditionTagNameEdit->text().trimmed());
    }
    if (kPointerMainWidget->ui->conditionJudgeComBox->currentIndex() == 0) {
        script->add(script->getopt(ConditionScript::ConditionYes), "1");
    } else {
        script->add(script->getopt(ConditionScript::ConditionNo), "1");
    }
    str += kPointerMainWidget->ui->conditionJudgeComBox->currentText();
    if (kPointerMainWidget->ui->continueRadioBtn->isChecked()) {
        script->add(script->getopt(ConditionScript::ContinueRadioBtn), "1");
        str += kPointerMainWidget->ui->continueRadioBtn->text();
    }
    if (kPointerMainWidget->ui->jmpToRadioBtn->isChecked()) {
        script->add(script->getopt(ConditionScript::JmpToRadioBtn), "1");
        script->add(script->getopt(ConditionScript::JmpIndexSpinBox), QString("%1").arg(kPointerMainWidget->ui->jmpIndexSpinBox->value()));
        str += kPointerMainWidget->ui->jmpToRadioBtn->text();
        str += QString("%1").arg(kPointerMainWidget->ui->jmpIndexSpinBox->value());
    }
    if (kPointerMainWidget->ui->waitRadioBtn->isChecked()) {
        script->add(script->getopt(ConditionScript::WaitRadioBtn), "1");
        str += kPointerMainWidget->ui->waitRadioBtn->text();
    }

    //将脚本添加到显示模型
    QList<QStandardItem*> item;
    item.push_back(new QStandardItem(cn_tr("条件判断")));
    item.push_back(new QStandardItem(str));
    immediateModel_->appendRow(item);
    //放到脚本列表中
    scriptControler_->addScript(script);
}
void CWriteScript::addVerifyScriptSlot    (){}//not use

void CWriteScript::addMoveMouseScriptSlot ()
{
    MouseMoveScript *script = new MouseMoveScript();
    QString str;
    QString strpos = kPointerMainWidget->ui->movePosEdit->text().trimmed();
    if (kPointerMainWidget->ui->movePosRadioBtn->isChecked() && !strpos.isEmpty()) {
        script->add(script->getopt(MouseMoveScript::MovePos), strpos);
        str += kPointerMainWidget->ui->movePosRadioBtn->text();
        str += strpos;
    }
    //将脚本添加到显示模型
    QList<QStandardItem*> item;
    item.push_back(new QStandardItem(cn_tr("鼠标移动")));
    item.push_back(new QStandardItem(str));
    immediateModel_->appendRow(item);
    //放到脚本列表中
    scriptControler_->addScript(script);
}
void CWriteScript::addKeyboardScriptSlot  (){}
void CWriteScript::addWindowOpScriptSlot  (){}
void CWriteScript::addJmpScriptSlot       (){}

void CWriteScript::addDelayScriptSlot     ()
{
    if (!kPointerMainWidget->ui->delayRadioBtn->isChecked() && !kPointerMainWidget->ui->delayRangeRadioBtn->isChecked()) {
        qWarning() << cn_tr("没有选择任何选项!");
        return;
    }
    QString str_delay = kPointerMainWidget->ui->delayTimeEdit->text().trimmed();
    QString str_delay_range = kPointerMainWidget->ui->delayRangeEdit->text().trimmed();

    if (str_delay.isEmpty() && str_delay_range.isEmpty()) {
        qWarning() << cn_tr("没有设置延时时间!");
        return;
    }
    DelayScript *script = new DelayScript();
    QString str;
    if (kPointerMainWidget->ui->delayRadioBtn->isChecked() && !str_delay.isEmpty()) {
        script->add(script->getopt(DelayScript::Delay), str_delay);
        str += kPointerMainWidget->ui->delayRadioBtn->text() + str_delay;
    }
    if (kPointerMainWidget->ui->delayRangeRadioBtn->isChecked() && !str_delay_range.isEmpty()) {
        script->add(script->getopt(DelayScript::DelayRange), str_delay_range);
        str += kPointerMainWidget->ui->delayRangeRadioBtn->text() + str_delay_range;
    }
    //将脚本添加到显示模型
    QList<QStandardItem*> item;
    item.push_back(new QStandardItem(cn_tr("延时等待")));
    item.push_back(new QStandardItem(str + cn_tr("毫秒")));
    immediateModel_->appendRow(item);
    //放到脚本列表中
    scriptControler_->addScript(script);
}
void CWriteScript::addScreenshotScriptSlot(){}
void CWriteScript::addCollectScriptSlot   (){}
void CWriteScript::addUploadScriptSlot    (){}


//定时器处理，捕获窗口
void CWriteScript::onGetArea()
{
    POINT pnt;
    RECT rc;
    HWND DeskHwnd = ::GetDesktopWindow(); //取得桌面句柄
    HDC DeskDC = ::GetWindowDC(DeskHwnd); //取得桌面设备场景
    int oldRop2 = ::SetROP2(DeskDC, R2_NOTXORPEN);
    ::GetCursorPos(&pnt); //取得鼠标坐标
    HWND UnHwnd = ::WindowFromPoint(pnt); //取得鼠标指针处窗口句柄
    hwnd_ = UnHwnd;

    HWND grayHwnd = ::GetWindow(hwnd_, GW_CHILD);
    RECT tempRc;
    bool bFind = false;
    while (grayHwnd) {
       ::GetWindowRect(grayHwnd, &tempRc);
       if (::PtInRect(&tempRc, pnt)) {
           bFind = true;
           break;
       } else {
           grayHwnd = ::GetWindow(grayHwnd, GW_HWNDNEXT);
       }
    }
    if (bFind == true) {
       bFind = false;
       hwnd_ = grayHwnd;
    }
    ::GetWindowRect(hwnd_, &rc); //获得窗口矩形
    if (rc.left < 0) {
        rc.left = 0;
    }
    if (rc.top < 0) {
        rc.top = 0;
    }
    HPEN newPen = ::CreatePen(0, 1, RGB(255,0,0)); //建立新画笔,载入DeskDC
    HGDIOBJ oldPen = ::SelectObject(DeskDC, newPen);
    ::Rectangle(DeskDC, rc.left, rc.top, rc.right, rc.bottom); //在指示窗口周围显示闪烁矩形
//------------------------------------------------------------------后续添加
    //获取窗口Title
    char strTitle[200]="\0";
    ::GetWindowTextA(hwnd_, strTitle, 200);
    QString str = QString::fromLocal8Bit((char*)strTitle); //处理中文，或者先转换为QByteArray 再转换为QString
    qDebug() << str;
    kPointerMainWidget->ui->windowTitleEdit->setText(str);

    //获取窗口类名
    char strClass[200]="\0";
    ::GetClassNameA(hwnd_,strClass,200);
    QString strTemp = cn_tr(strClass);
    //QMessageBox::information(NULL, cn_tr("类名"), strTemp);
    kPointerMainWidget->ui->windowTypeEdit->setText(strTemp);

    //QString strRect = QString("(%1,%2),(%3,%4) %5x%6").arg(rc.left).arg(rc.top).arg(rc.right).arg(rc.bottom).arg(rc.right-rc.left).arg(rc.bottom-rc.top);
    //windowRect->setText(strRect);
//------------------------------------------------------------------
    QTime dieTime = QTime::currentTime().addMSecs(400); //设置闪烁时间间隔
    while (QTime::currentTime() < dieTime) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }

    ::Rectangle(DeskDC, rc.left, rc.top, rc.right, rc.bottom);

    ::SetROP2(DeskDC, oldRop2);
    ::SelectObject(DeskDC, oldPen);
    ::DeleteObject(newPen);
    ::ReleaseDC(DeskHwnd, DeskDC);
    DeskDC = NULL;
}


void CWriteScript::runScriptGroupSlot()
//运行脚本组
{
    scriptControler_->runGroup();
}

void CWriteScript::runOneStepSlot()
//单步运行
{    qDebug("runOneStepSlot");
    //取得选定行的索引,索引的位置相当于在 scriptControler_ 对象中 list 向量的索引
    QModelIndex index = kPointerMainWidget->ui->immScriptTable->currentIndex(); //从0行开始,与m_scriptList 中的索引一样
    if (!index.isValid()) {QMessageBox::warning(NULL, cn_tr("提示"), cn_tr("请先选择一行数据!"));  return;}
    //执行相应操作
    scriptControler_->run(index.row());
}

void CWriteScript::saveScriptSlot()
{
    QString filename = QFileDialog::getSaveFileName(kPointerMainWidget, cn_tr("保存脚本"), "./", "scripts(*.sc)");
    scriptControler_->save(filename);
}

void CWriteScript::loadScriptSlot()
{
    QString filename = QFileDialog::getOpenFileName(kPointerMainWidget, cn_tr("导入脚本"), "./", "scripts(*.sc)");
    scriptControler_->load(filename);
}

/*
删除选定脚本
*/
void CWriteScript::deleteScriptSlot()
{
    QModelIndex index = kPointerMainWidget->ui->immScriptTable->currentIndex();
    if (!index.isValid()) {
        QMessageBox::warning(kPointerMainWidget, cn_tr("提示"), cn_tr("没有选择任何脚本!"));
        return;
    }
    //删除显示模型上的
    immediateModel_->removeRow(index.row());
    //删除内部列表中的
    scriptControler_->delScript(index.row());
}
/*
清空脚本列表
*/
void CWriteScript::clearAllScriptSlot()
{
    int count = immediateModel_->rowCount();
    while (count-- > 0) {
        QModelIndex index = kPointerMainWidget->ui->immScriptTable->indexAt(QPoint(count, 0));
        immediateModel_->removeRow(index.row());
        scriptControler_->delScript(index.row());
    }
}

void CWriteScript::moveUpSlot()
{
    QModelIndex index = kPointerMainWidget->ui->immScriptTable->currentIndex();
    if (index.row() == 0) { //最顶行不能再上移了
        return;
    }
    QList<QStandardItem *> itemlist1 = immediateModel_->takeRow(index.row());
    QList<QStandardItem *> itemlist2 = immediateModel_->takeRow(index.row()-1);
    immediateModel_->insertRow(index.row()-1, itemlist1);
    immediateModel_->insertRow(index.row(), itemlist2);
    scriptControler_->swapScript(index.row(), index.row()-1);
}
void CWriteScript::moveDownSlot()
{
    QModelIndex index = kPointerMainWidget->ui->immScriptTable->currentIndex();
    if (index.row() == immediateModel_->rowCount()-1) { //最底行不能再下移了
        return;
    }
    QList<QStandardItem *> itemlist2 = immediateModel_->takeRow(index.row()+1);
    QList<QStandardItem *> itemlist1 = immediateModel_->takeRow(index.row());
    immediateModel_->insertRow(index.row(), itemlist2);
    immediateModel_->insertRow(index.row()+1, itemlist1);
    scriptControler_->swapScript(index.row(), index.row()+1);
}
void CWriteScript::moveTopSlot()
{
//...
}
void CWriteScript::moveBottomSlot()
{
//...
}
void CWriteScript::clearTagInfoSlot()
{
    kPointerMainWidget->ui->tagTagNameEdit->clear();
    kPointerMainWidget->ui->tagContentEdit->clear();
    kPointerMainWidget->ui->tagTypeEdit->clear();
    kPointerMainWidget->ui->tagClassEdit->clear();
    kPointerMainWidget->ui->tagIdEdit->clear();
    kPointerMainWidget->ui->tagTitleEdit->clear();
    kPointerMainWidget->ui->tagNameEdit->clear();
    kPointerMainWidget->ui->tagValueEdit->clear();
    kPointerMainWidget->ui->tagSrcEdit->clear();
    kPointerMainWidget->ui->tagHrefEdit->clear();
}

CWriteScript::~CWriteScript()
{
    browserGet_->uninstallWebEventsHandle();
    delete browserGet_;
    delete immediateModel_;
    delete getAreatimer_;
    delete scriptControler_;
    delete remoteVerify_;
    delete findElement_;
}




