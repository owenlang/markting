#ifndef KEYBOARDOPSCRIPT_H
#define KEYBOARDOPSCRIPT_H

#include "cscript.h"

QT_BEGIN_NAMESPACE
class QObject;
class QString;
QT_END_NAMESPACE

class KeyboardOpScript : public CScript
{
    Q_OBJECT
public:
    typedef enum {
        Start = 0,
            Key,
        End
    }KeyboardOption;
public:
    explicit KeyboardOpScript(QObject *parent = 0);
    virtual ~KeyboardOpScript();
    virtual void run();
    virtual void stop();
    virtual QString getopt(const KeyboardOption& option);
signals:
    
public slots:
    
};

#endif // KEYBOARDOPSCRIPT_H
