#ifndef FINDELEMENT_H
#define FINDELEMENT_H

#include <QWidget>
#include "ui_findelement.h"

class BrowserGet;

class FindElement : public QWidget, public Ui::FindElement
{
    Q_OBJECT
public:
    explicit FindElement(QWidget *parent = 0);
    virtual ~FindElement();
signals:
    
public slots:
    void on_findBtn_clicked();
    void on_okBtn_clicked();    
    void on_showBtn_clicked();

private:
    QString keyword_;
    BrowserGet *browserGet_;
    QString htmlText_;
};

#endif // FINDELEMENT_H
