/*-----------------------------------------------------------------------------
Description  : 接收处理 DIID_HTMLDocumentEvents2 事件的类
Author : WRW.1229567637@qq.com
Date : 2013/11/06
-----------------------------------------------------------------------------*/
#include "StdAfx.h"
#include "HTMLDocEvent.h"
#include "ui_MainWidget.h"

HTMLDocEvent::HTMLDocEvent(QObject *parent) : QObject(parent)
{
    m_dwCookie = 0;
    m_pPoint = NULL;
    timer_ = new QTimer(this);
    QObject::connect(timer_, SIGNAL(timeout()), this, SLOT(timeOut()));
    spElement_ = NULL;
    spNewElem_ = NULL;
    spOldElem_ = NULL;
}

HRESULT  HTMLDocEvent::GetTypeInfoCount(
            /* [out] */ UINT *pctinfo)
{
    pctinfo=pctinfo;
    return E_NOTIMPL;
}

HRESULT  HTMLDocEvent::GetTypeInfo(
    /* [in] */ UINT iTInfo,
    /* [in] */ LCID lcid,
    /* [out] */ ITypeInfo **ppTInfo)
{
    iTInfo=iTInfo; lcid=lcid; ppTInfo=ppTInfo;
    return E_NOTIMPL;
}

HRESULT  HTMLDocEvent::GetIDsOfNames(
    /* [in] */ REFIID riid,
    /* [size_is][in] */ LPOLESTR *rgszNames,
    /* [in] */ UINT cNames,
    /* [in] */ LCID lcid,
    /* [size_is][out] */ DISPID *rgDispId)
{
    rgszNames = rgszNames; cNames = cNames; lcid = lcid; rgDispId = rgDispId;
    return E_NOTIMPL;
}

//卸载 DIID_HTMLDocumentEvents2 事件接收器
void HTMLDocEvent::uninstallDocEventsHandle()
{
	//如果还存在连接，则断开连接
    if (m_dwCookie || m_pPoint) {
        qDebug("Exist DIID_HTMLDocumentEvents2 connect point, disconnect it.");
        m_pPoint->Unadvise(m_dwCookie);
        m_dwCookie = 0;
        m_pPoint = NULL;
        if (kPointerMainWidget) {
            kPointerMainWidget->ui->probeBtn->setText(cn_tr("探测"));
        }
    }
}
/*
 <INPUT id=kw class=s_ipt style="BORDER-TOP: red 1px solid; BORDER-RIGHT: red 1px solid; BORDER-BOTTOM:
 red 1px solid; BORDER-LEFT: red 1px solid" maxLength=100 name=wd autocomplete="off">

 <INPUT onmousedown="this.className='bg s_btn s_btn_h'" onmouseout="this.className='bg s_btn'"
id=su class="bg s_btn" style="BORDER-TOP: red 1px solid; BORDER-RIGHT:
 red 1px solid; BORDER-BOTTOM: red 1px solid; BORDER-LEFT: red 1px solid" type=submit value=百度一下>
 */
void HTMLDocEvent::clearTagInfo()
{
    kPointerMainWidget->ui->tagTagNameEdit->clear();
    kPointerMainWidget->ui->tagContentEdit->clear();
    kPointerMainWidget->ui->tagTypeEdit->clear();
    kPointerMainWidget->ui->tagClassEdit->clear();
    kPointerMainWidget->ui->tagIdEdit->clear();
    kPointerMainWidget->ui->tagTitleEdit->clear();
    kPointerMainWidget->ui->tagNameEdit->clear();
    kPointerMainWidget->ui->tagValueEdit->clear();
    kPointerMainWidget->ui->tagSrcEdit->clear();
    kPointerMainWidget->ui->tagHrefEdit->clear();
    kPointerMainWidget->ui->tagAllTextEdit->clear();
}

void HTMLDocEvent::updateInfo(IHTMLElement *spElem)
{
    if (!kPointerMainWidget) {
        return;
    }
    clearTagInfo(); /*先清除原来的信息*/
    QString val;    /*获取元素信息*/
    ElementInfo element(spElem);
    val = element.getAttrValue("tagname");
    kPointerMainWidget->ui->tagTagNameEdit->setText(val);
    val = element.getAttrValue("type");
    kPointerMainWidget->ui->tagTypeEdit->setText(val);
    val = element.getAttrValue("class");
    kPointerMainWidget->ui->tagClassEdit->setText(val);
    val = element.getAttrValue(cn_tr("内容"));
    kPointerMainWidget->ui->tagContentEdit->setText(val);
    val = element.getAttrValue("value");
    kPointerMainWidget->ui->tagValueEdit->setText(val);
    val = element.getAttrValue("id");
    kPointerMainWidget->ui->tagIdEdit->setText(val);
    val = element.getAttrValue("name");
    kPointerMainWidget->ui->tagNameEdit->setText(val);
    val = element.getAttrValue("src");
    kPointerMainWidget->ui->tagSrcEdit->setText(val);
    val = element.getAttrValue("href");
    kPointerMainWidget->ui->tagHrefEdit->setText(val);
    val = element.getAttrValue("title");
    kPointerMainWidget->ui->tagTitleEdit->setText(val);

    HRESULT hr;
    _bstr_t bsTag;
    hr = spElem->get_outerHTML(bsTag.GetAddress());
    if (SUCCEEDED(hr)) {
        QString htmltext = QString::fromWCharArray(bsTag.GetBSTR());
        htmltext.replace(QRegExp(">\\s+<"), "><");  //clear write symbol between tag
        htmltext.replace(QRegExp("\\s+"), " "); //clear other too long write symbol
        kPointerMainWidget->ui->tagAllTextEdit->setPlainText(htmltext);
    }    
}

HRESULT  HTMLDocEvent::Invoke(    /* [in] */ DISPID dispIdMember,    /* [in] */ REFIID riid,    /* [in] */ LCID lcid,
    /* [in] */ WORD wFlags,    /* [out][in] */ DISPPARAMS *pDispParams,    /* [out] */ VARIANT *pVarResult,    /* [out] */ EXCEPINFO *pExcepInfo,    /* [out] */ UINT *puArgErr)
{
    lcid=lcid; wFlags=wFlags; pVarResult=pVarResult; pExcepInfo=pExcepInfo; puArgErr=puArgErr;
    USES_CONVERSION;
    if (!pDispParams) {
        return E_INVALIDARG;
    }
	HRESULT hr;
	CComPtr <IDispatch> spEventDisp;
    spEventDisp = pDispParams->rgvarg[0].pdispVal;
    CComQIPtr <IHTMLEventObj2, &IID_IHTMLEventObj2> spEventObj2;
    hr = spEventDisp->QueryInterface(IID_IHTMLEventObj2, (void**)&spEventObj2);
    CComQIPtr <IHTMLEventObj, &IID_IHTMLEventObj> spevtobj(spEventObj2);

    switch (dispIdMember) {    
    case DISPID_HTMLDOCUMENTEVENTS2_ONCLICK:
        {
            if (spEventObj2 && spevtobj) {
                CComQIPtr <IHTMLElement, &IID_IHTMLElement> spElement;
                hr = spEventObj2->get_srcElement(&spElement);
                if (SUCCEEDED(hr)) {
                    ElementInfo info(spElement);
                    QString url = info.getAttrValue("href");
                    if (!url.isEmpty()) {
                        BrowserGet *browser = BrowserGet::getInstance();
                        browser->openUrl(url);
                    }
                }
            }
        }
        break;
    case DISPID_HTMLDOCUMENTEVENTS_ONCONTEXTMENU:
        {
            if (spEventObj2 && spevtobj && (kProbeState==mark::USE_PROBE)) {
                kProbeState = mark::NO_PROBE;
                kPointerMainWidget->ui->probeBtn->setText(cn_tr("探测"));
                spevtobj->put_returnValue(CComVariant(false));
                drawRestore();
                timer_->stop();                
                break;
            }
        }
        break;
    case DISPID_HTMLDOCUMENTEVENTS2_ONMOUSEMOVE://鼠标移动时,探测并绘制矩形
        {
            if (spEventObj2 && kProbeState==mark::USE_PROBE) {
                hr = spEventObj2->get_srcElement(&spElement_); //鼠标所在位置的标记
                if (SUCCEEDED(hr) && spElement_) {
                    updateInfo(spElement_);//更新到主界面
                    beginDraw(spElement_);
                }
            }
        }
        break;
    case DISPID_QUIT:
        uninstallDocEventsHandle();
        break;
    }
    return S_OK;
}

void HTMLDocEvent::beginDraw(IHTMLElement *spElem)
{
    if (spNewElem_ != spElem) {
        spNewElem_ = spElem;
        if (spOldElem_) {
            drawRestore();
            timer_->stop();
        }
        CComPtr <IHTMLStyle> spstyle;
        spNewElem_->get_style(&spstyle);
        spstyle->get_border(border.GetAddress());
        if (spOldElem_ != spNewElem_) {
            spOldElem_ = spNewElem_;
        }
        timer_->start(300);
    }
}

void HTMLDocEvent::timeOut()
{
    static bool bval = false;
    CComPtr <IHTMLStyle> spstyle;
    spNewElem_->get_style(&spstyle);
    if (bval) {
        spstyle->put_border(border.GetBSTR());
    } else {
        spstyle->put_border(_bstr_t("1px solid #F00"));
    }
    bval = !bval;
}

void HTMLDocEvent::drawRestore()
{
    CComPtr <IHTMLStyle> spstyle;
    spOldElem_->get_style(&spstyle);
    spstyle->put_border(border.GetBSTR());
}

HTMLDocEvent::~HTMLDocEvent()
{
    spElement_->Release();
    spNewElem_->Release();
    spOldElem_->Release();
    spElement_ = NULL;
    spNewElem_ = NULL;
    spOldElem_ = NULL;
    delete timer_;
}


#if 0
HWND HTMLDocEvent::GetHwndFromIWebBrowser2(IWebBrowser2* pWebBrowser2)
//must include <ShlGuid.h>
{
    if (pWebBrowser2 == NULL)
        return NULL;
    IServiceProvider* pServiceProvider = NULL;
    //1. --Shell Embedding
    if (SUCCEEDED(pWebBrowser2->QueryInterface(IID_IServiceProvider, (void**)&pServiceProvider))) {
        IOleWindow* pWindow = NULL;
        if (SUCCEEDED(pServiceProvider->QueryService(SID_SShellBrowser, IID_IOleWindow, (void**)&pWindow))) {
            HWND hwndBrowser = NULL;
            if (SUCCEEDED(pWindow->GetWindow(&hwndBrowser))) {
                //2.|-Shell DocObject View
                HWND hchildwnd = GetWindow(hwndBrowser, GW_CHILD);
                while (hchildwnd) {
                    TCHAR wndname[MAX_PATH] = _T("");
                    GetClassName(hchildwnd, wndname, MAX_PATH);
                    if ( wcscmp(wndname,_T("Shell DocObject View")) == 0 ) {
                        //3.|--Internet Explorer_Server
                         HWND hiewnd = GetWindow(hchildwnd, GW_CHILD);
                        while (hiewnd) {
                            TCHAR wndname[MAX_PATH] = _T("");
                            GetClassName(hiewnd, wndname, MAX_PATH);
                            if ( wcscmp(wndname,_T("Internet Explorer_Server")) == 0 ) {
                                return hiewnd;
                            }
                            hiewnd = GetNextWindow(hiewnd, GW_HWNDNEXT);
                        }
                        return hwndBrowser;
                    }
                    hchildwnd = GetNextWindow(hchildwnd, GW_HWNDNEXT);
                }
                return hwndBrowser;
            }
            pWindow->Release();
        }
        pServiceProvider->Release();
    }
    return NULL;
}
#endif
