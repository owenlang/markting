#ifndef MOUSEMOVESCRIPT_H
#define MOUSEMOVESCRIPT_H

#include "CScript.h"

QT_BEGIN_NAMESPACE
class QString;
class QObject;
QT_END_NAMESPACE

class MouseMoveScript : public CScript
{
    Q_OBJECT
public:
    typedef enum{
        Start = 0,
            MovePos,
        End
    }MouseOption;
public:
    explicit MouseMoveScript(QObject *parent = 0);
    virtual ~MouseMoveScript();
    void initData();
    void run();
    void stop();
    QString getopt(const MouseOption &option);
signals:
    void runFinish(mark::RunDirect rd, int to);
    
public slots:    

};

#endif // MOUSEMOVESCRIPT_H
