进度			√)表示已经完成	＋)表示正在开发	－)表示还未完成

一.界面制作
换肤功能		---SkinStyle
配置部署		---ConfigDeploy
界面美化

二.编写脚本
√访问网页		---AccessWebScript
√网页填表		---FillWebScript
√探测网页元素
√点击元素
√设置元素属性
√设置内容
√验证码
√延时等待		---DelayScript
√条件判断		---ConditionScript
＋脚本跳转		---JmpScript
－移动鼠标		---MouseMoveScript
－键盘操作		---KeyboardOpScript
－窗口操作		---WindowOpScript
－提取采集		---CollectScript
－上传功能		---UploadScript
－截图功能		---ScreenshotScript

三.文本管理
＋变量管理		---VariableManager

四.脚本管理
－保存脚本
－导入脚本
－编辑脚本

六.服务器端

七.帐户管理

八.软件加密
        
        

设计
现有类:
BrowserEvent    浏览器事件捕获类
HTMLDocEvent    浏览器文档事件捕获类

SandboxLabel    绑定窗口类
SysButton       系统右上角按钮

CScript         脚本格式--分到各种脚本类分别处理

改进设计:
    全局常量:脚本类型
    全局变量:主界面类的指针(用于其它类更新显示)

MainWidget      主界面类:关联以下类
    TitleWidget     标题栏类
    SysButton       系统右上角按钮类
    配置文件类,换肤类
    open ie 类

    WriteScript编写脚本类:关联脚本编写需要的所有东西,主界面类创建这个对象,又关联以下类
        创建脚本类:根据用户的选项创建相应的脚本对象
        运行脚本类(控制脚本运行):创建出来的脚本对象放到这个控制类中
        各种脚本类:各种类型的脚本

        BrowserGet 获取浏览器接口类
                BrowserEvent    浏览器事件捕获类
                HTMLDocEvent    浏览器文档事件捕获类
        SandboxLabel    绑定窗口类
        
        获取网络图片类,IE代理类

现在没有使用的类
ProbeLabel      探测的图标类
IE代理类 :IEProxy 类


